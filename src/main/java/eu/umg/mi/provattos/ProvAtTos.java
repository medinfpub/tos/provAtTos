package eu.umg.mi.provattos;

import org.apache.commons.cli.*;
import eu.umg.mi.provattos.modules.TOSWrapper;

import java.io.File;
import java.util.List;

import javax.naming.ConfigurationException;

public class ProvAtTos {

    static String shellCommand = "java -jar target/provAtTos.jar [OPTIONS] [JOB]";

    public static void main(String[] args) {
        Options options = new Options();

        // add options
        options.addOption("f", "format", true, "Specify a output format for captured provenance. Available formats: xml, provn (default), turtle");
        options.addOption("o", "output", true, "The output directory for captured provenance data");
        options.addOption("t", "tos", true, "Specify arguments that will be given to TOS, e.g. \"--context_param=Default\"");
        options.addOption("n", "namespace", true, "Sets a namespace for elements that reference local objects, e.g. local files");

        // Parse the commandline arguments
        HelpFormatter help = new HelpFormatter();
        CommandLineParser parser = new DefaultParser();
        CommandLine line;
        try {
            line = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Parsing of the commandline arguments failed.");
            help.printHelp(ProvAtTos.shellCommand, options);
            return;
        }

        List<String> leftover = line.getArgList();
        // the leftoverargument need to be at least 1 (for the TOS job)
        if (leftover.size() < 1) {
            System.err.println("Missing a JOB parameter to continue. Please provide either a .zip-File or a extraction-directory of an exported TOS-Job.");
            help.printHelp(ProvAtTos.shellCommand, options);
            // 64 for command line usage error (as of /usr/include/sysexits.h)
            System.exit(64);
        }

        // Construct the tWrapper, unzip the TOS job if needed
        String argumentPath = leftover.remove(0);
        // The main classes name can be provided, but is not needed. Throw it away if provided.
        if(argumentPath.equals("ProvAtTos")) {
            argumentPath = leftover.remove(0);
        }
        // expand ~ if necessary (shell special character at the beginning, denotes users home directory on unix)
        if (argumentPath.startsWith("~")) {
            argumentPath = System.getProperty("user.home") + argumentPath.substring(1);
        }
        TOSWrapper tWrapper = new TOSWrapper(argumentPath);
        if (tWrapper.isErroneous()) {
            System.err.println("Initializing the TOS job failed.");
            // 66 for cannot open input (as of /usr/include/sysexits.h)
            System.exit(66);
        }

        // Check if an output file has been provided. If not, no output file will be created.
        String outputPath = System.getProperty("user.dir");
        if (line.hasOption("o")) {
            outputPath = line.getOptionValue("o");
            // expand ~ if necessary (shell special character at the beginning, denotes users home directory)
            if (outputPath.startsWith("~")) {
                outputPath = System.getProperty("user.home") + outputPath.substring(1);
            }
        }
        tWrapper.initOutputFile(new File(outputPath));
        if (tWrapper.isErroneous()) {
            System.err.println("The specified output dir is invalid or not writable");
            help.printHelp(ProvAtTos.shellCommand, options);
            tWrapper.close();
            // 73 for cannot create (user) output file (as of /usr/include/sysexits.h)
            System.exit(73);
        }

        // set the provenance format
        String provFormat = "provn";
        if(line.hasOption("f")) {
            provFormat = line.getOptionValue("f");
        }
        tWrapper.setProvenanceFormat(provFormat);

        // set a namespace if available; if not set, example.com is used
        if(line.hasOption("n")) {
            tWrapper.setDefaultNamespace(line.getOptionValue("n"));
        }

        // set possible further arguments for the job
        String[] arguments = new String[0];
        if(line.hasOption("t")) {
            String allArguments = line.getOptionValue("t");
            arguments = allArguments.split(" ");
        }

        // run the job with the supplied arguments
        System.out.println("Starting " + argumentPath);
        try {
            tWrapper.run(arguments);
        } catch(ConfigurationException e) {
            System.err.println(e.getMessage());
            tWrapper.close();
            // 66 for cannot open input (as of /usr/include/sysexits.h)
            System.exit(66);
        }
        if (tWrapper.isErroneous()) {
            System.err.println("Running the job failed, capturing provenance most probably did not happen. Check the output for the status of the Talend Open Studio job.");
        } else {
            System.out.println("Running the job finished.");
        }

        // close the connections at the end
        tWrapper.close();
    }
}
