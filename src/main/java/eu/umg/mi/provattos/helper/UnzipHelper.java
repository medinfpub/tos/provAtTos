package eu.umg.mi.provattos.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipInputStream;

public class UnzipHelper {

    public static File unzip(File path) throws IOException {
        byte[] buf = new byte[1024];

        if (!path.isFile()) {
            throw new IOException("Provided path is not a file, aborting.");
        }

        File extractedPath;
        FileInputStream filestream;
        ZipInputStream zipInput;
        try {
            String foldername = path.getParent()
                    + File.separator
                    + path.getName().substring(0, path.getName().length() - 4).replace(".", "-");
            extractedPath = new File(foldername);
            int i = 1;
            while (extractedPath.exists()) {
                extractedPath = new File(foldername + "_" + i);
                i += 1;
            }
            extractedPath.mkdirs();

            filestream = new FileInputStream(path);
            zipInput = new ZipInputStream(filestream);

            ZipEntry zipE = zipInput.getNextEntry();

            while (zipE != null) {
                String extractName = zipE.getName();
                File extractFile = new File(extractedPath, extractName);

                new File(extractFile.getParent()).mkdirs();
                if (!zipE.isDirectory()) {
                    FileOutputStream fos = new FileOutputStream(extractFile);

                    int fileSize;
                    while ((fileSize = zipInput.read(buf)) > 0) {
                        fos.write(buf, 0, fileSize);
                    }
                    fos.close();
                }

                zipE = zipInput.getNextEntry();
            }

            zipInput.closeEntry();
            zipInput.close();
            filestream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // TODO: do something;
            return null;

        } catch (SecurityException e) {
            e.printStackTrace();
            return null;
            // TODO: do something

        } catch (ZipException e) {
            System.err.println("Zip file seems corrupted.");
            e.printStackTrace();
            // TODO: do something
            return null;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
            // TODO: do something

        }

        return extractedPath;
    }

}
