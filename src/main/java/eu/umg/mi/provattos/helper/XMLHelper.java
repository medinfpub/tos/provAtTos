package eu.umg.mi.provattos.helper;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLHelper {

    private Document propertiesItem;
    private File outputPath;
// TODO: dirty! see AGGIN and AGGOUT handling
    private int intermediateNum = 0;
    private String filename;

    public XMLHelper(File outputPath, File itemPath, File itemFileName) throws IllegalArgumentException {
        this.outputPath = outputPath;
        this.propertiesItem = this.findPropertiesItem(itemPath, itemFileName);
        if(this.propertiesItem == null) {
            throw new IllegalArgumentException("The XMLHelper is not able to retrieve a document using this arguments: Path = " + itemPath.toString() + ", Filename = " + itemFileName.toString());
        }
    }

    public String getFilename() {
        return this.filename;
    }

    public File getOutputPath() {
        return this.outputPath;
    }

    /**
     * Finds a properties item with the name itemFileName within the path itemPath, parses the XML and returns a parsed Document
     * @param itemPath The path that will be used as a starting point to search
     * @param itemFileName The name of the .item-XML file that the method should look for
     * @return A Document instance that represents the .item-XML file, which is the main source of information for an XMLHelper instance
     */
    private Document findPropertiesItem(File itemPath, File itemFileName) {
        // The first objective is to find the file within itemPath using the Files.find() method
        String itemFileNameString = itemFileName.toString();
        Path absolutePath;
        try {
            // Find the file and capture possible errors like IOExceptions or non-present files
            Stream<Path> streamOfPaths = Files.find(itemPath.toPath(), Integer.MAX_VALUE, new BiPredicate<Path,BasicFileAttributes>(){
                @Override
                public boolean test(Path t, BasicFileAttributes u) {
                    if(t.getFileName().toString().equals(itemFileNameString)) {
                        return true;
                    }
                    return false;
                }
            }, FileVisitOption.FOLLOW_LINKS);

            // the optional will indicate whether a file has been found or not
            Optional<Path> optPath = streamOfPaths.findFirst();
            if(!optPath.isPresent()) {
                System.err.println("Cannot find the .item-XML file \"" + itemFileName + "\"");
                streamOfPaths.close();
                return null;
            }
            absolutePath = optPath.get();
            streamOfPaths.close();
        } catch(IOException e) {
            System.err.println("Not able to access files at path \"" + itemPath + "\"");
            return null;
        }

        // if the file is found, check if it is a file and it is readable
        File componentFile = new File(absolutePath.toString());
        if(!componentFile.isFile() || !componentFile.canRead()) {
            System.err.println("Found, but cannot read the .item-XML file \"" + itemFileName + "\"");
        }
        this.filename = componentFile.getName();

        // use the xmlBuilder from the DocumentBuilderFactory to create an internal Document for the XML-file, catching all possible exceptions
        DocumentBuilder xmlBuilder;
        try {
            DocumentBuilderFactory xmlFac = DocumentBuilderFactory.newInstance();
            xmlFac.setNamespaceAware(true);
            xmlBuilder = xmlFac.newDocumentBuilder();

            // if built successfully, return the document
            return xmlBuilder.parse(componentFile);
        } catch (ParserConfigurationException e) {
            System.err.println("Cannot parse the items-xml file");
        } catch (SAXException e) {
            System.err.println("An SAX exception occurred: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Cannot open the items-xml file");
        }
        return null;
    }

    /**
     * Retrieves a the mapperData from the .item-XML file regarding the node with the unique name nodeName
     * @param nodeName unique name of the node that will be retrieved
     * @return A File instance to the created XML file containing the mapperData
     */
    public File extractPlanForMap(String nodeName, String jobName) {
        Transformer transformer;
        Node nd = this.getMapperNodeByName(nodeName);
        DOMSource src = new DOMSource(nd);
        File saveTo = new File(this.outputPath + File.separator + jobName + "_" + nodeName + "_mapper.xml");
        StreamResult res = new StreamResult(saveTo);
        try {
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, res);
        } catch (TransformerConfigurationException e) {
            System.err.println("Could not create a new Transformer instance.");
            return null;
        } catch (TransformerException e) {
            System.err.println("Cannot transform the mapping parameters to XML: " + e.getMessage());
            return null;
        }

        return saveTo;
    }

    /**
     * Gets all components that have a connection to a component.
     * 
     * This function retrieves all component names that are connected to the TOS-job component with the name componentName according to the properties.item XML file
     * 
     * @param componentName A String representation of the componentName, equivalent to the UNIQUE LABEL in TOS
     * @return              A list of Strings representing unique labels of connected inputs in TOS
     */
    public List<NamedNodeMap> getConnectedFrom(String componentName) {
        List<org.w3c.dom.NamedNodeMap> allSources = new ArrayList<>();
        // TODO: dirty! get AGGOUTs and map them manually to AGGINs
        if(componentName.endsWith("_AGGIN")) {
            // create an element and add it to the sources, very dirty way to do that
            org.w3c.dom.Element e = this.propertiesItem.createElement("connection");
            e.setAttribute("label", "intermediate" + this.intermediateNum);
            this.intermediateNum++;
            e.setAttribute("source", componentName.substring(0, componentName.length()-6) + "_AGGOUT");
            allSources.add(e.getAttributes());
            return allSources;
        }
        // TODO: dirty! get AGGINs and change them to the unique label only
        if(componentName.endsWith("_AGGOUT")) {
            componentName = componentName.substring(0, componentName.length()-7);
        }

        // retrieve the sources from the .item-XML file using xPath
        String xpathExprStr = "talendfile:ProcessType/connection[@target='"+componentName+"']";
        NodeList sources = (NodeList) this.genericXPathFromProperties(xpathExprStr, XPathConstants.NODESET);
        if (sources == null) {
            return null;
        }

        // iterate through them and return a list
        for(int i = 0; i < sources.getLength(); i++) {
            allSources.add(sources.item(i).getAttributes());
        }
        return allSources;
    }

    /**
     * @deprecated Retrieves a single (the first) component that is not a target of any connection. Deprecation warning: this is incorrect and should not be used anymore!
     */
    public String getStartingActivity() {
        NodeList subjobNodes = (NodeList) this.genericXPathFromProperties("talendfile:ProcessType/subjob/elementParameter[@name='UNIQUE_NAME']/@value", XPathConstants.NODESET);
        Node n;
        if(subjobNodes == null) {
            return null;
        }
        for(int i = 0; i < subjobNodes.getLength(); i++) {
            n = subjobNodes.item(i);
            NodeList connectionsTo = (NodeList) this.genericXPathFromProperties("talendfile:ProcessType/connection[@target='" + n.getNodeValue() + "']", XPathConstants.NODESET);
            if(connectionsTo != null && connectionsTo.getLength() == 0) {
                return n.getNodeValue();
            }
        }
        return null;
    }

    /**
     * Retrieves a list of all starting components of each subjob, which is not targeted by any connection.
     * @return A list of strings that corrospon to the unique names of the components which start a subjob without being triggered by another component (directly)
     */
    public List<String> getStartingActivities() {
        NodeList subjobNodes = (NodeList) this.genericXPathFromProperties("talendfile:ProcessType/subjob/elementParameter[@name='UNIQUE_NAME']/@value", XPathConstants.NODESET);
        Node n;
        if(subjobNodes == null) {
            return null;
        }
        List<String> startActs = new ArrayList<String>();
        for(int i = 0; i < subjobNodes.getLength(); i++) {
            n = subjobNodes.item(i);
            NodeList connectionsTo = (NodeList) this.genericXPathFromProperties("talendfile:ProcessType/connection[@target='" + n.getNodeValue() + "']", XPathConstants.NODESET);
            if(connectionsTo != null && connectionsTo.getLength() == 0) {
                startActs.add(n.getNodeValue());
            }
        }
        return startActs;
    }

    /**
     * A generic function that retrieves an element from the elementParameters from the properties.items XML-file for a node with the unique name nodeName
     * @param nodeName The unique name of a component
     * @param element The name of the element which value should be retrieved
     * @return The String representation of the value of element from nodeName
     */
    public String getElementByNodeName(String nodeName, String element) {
        String xPath = "talendfile:ProcessType/node[elementParameter/@name='UNIQUE_NAME' and elementParameter/@value='" + nodeName + "']/elementParameter[@name='" + element + "']/@value";
        return (String) this.genericXPathFromProperties(xPath, XPathConstants.STRING);
    }

    /**
     * Parses all elementParameter from the .item-XML file for the node with the UNIQUE_NAME nodeName and retrieves it as a Properties instance
     * @param nodeName Name of the node from which the elementParameter values are parsed
     * @return A java.util.Properties instance of all parameters regarding the node nodeName
     */
    public Properties getNodeElementParameters(String nodeName) {
        String xPath = "talendfile:ProcessType/node[elementParameter/@name='UNIQUE_NAME' and elementParameter/@value='" + nodeName + "']/elementParameter[@field!='MEMO_JAVA']";
        NodeList parameters = (NodeList) this.genericXPathFromProperties(xPath, XPathConstants.NODESET);
        return nodeListToProperties(parameters, "name", "value");
    }

    public Properties getNodeJavaCodeParameters(String nodeName) {
        String xPath = "talendfile:ProcessType/node[elementParameter/@name='UNIQUE_NAME' and elementParameter/@value='" + nodeName + "']/elementParameter[@field='MEMO_JAVA']";
        NodeList javaParams = (NodeList) this.genericXPathFromProperties(xPath, XPathConstants.NODESET);
        return nodeListToProperties(javaParams, "name", "value");
    }

    public Properties nodeListToProperties(NodeList nl, String key, String value) {
        Properties props = new Properties();
        if(nl == null) {
            return props;
        }
        for(int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);
            if(n.hasAttributes()) {
                Node keyN = n.getAttributes().getNamedItem(key);
                Node valN = n.getAttributes().getNamedItem(value);
                if(keyN != null && valN != null) {
                    props.setProperty(keyN.getNodeValue(), valN.getNodeValue());
                }
            }
        }
        return props;
    }

    /**
     * Retrieves the TalendMapper:MapperData from the node with the unique name nodeName from the .item-XML-file
     * @param nodeName Unique name of the node
     * @return nodeData Node from nodeName
     */
    private Node getMapperNodeByName(String nodeName) {
        String xPath = "talendfile:ProcessType/node[elementParameter/@name='UNIQUE_NAME' and elementParameter/@value='" + nodeName + "']/nodeData";
        return (Node) this.genericXPathFromProperties(xPath, XPathConstants.NODE);
    }

    /**
     * Generic xPath query on the .item-XML file for a job. Retrieves and Object according to a QName from XPathConstants (e.g. STRING or NODESET)
     * @param xpath The xPath String that should be retrieved from the XML-file
     * @param xpathType The xPath return type, should be applicable to xPath.evaluate(String, Document, QName)
     * @return An object that is returned from xPath.evaluate(String, Document, QName) on success, null otherwise
     */
    private Object genericXPathFromProperties(String xpath, QName xpathType) {
        XPath xPath = XPathFactory.newInstance().newXPath();
        Object result;
        try {
            xPath.setNamespaceContext(new TalendNamespaceContext(this.propertiesItem));
            result = xPath.evaluate(xpath, this.propertiesItem, xpathType);
        } catch (XPathExpressionException e) {
            System.err.println("XPath could not be executed for .item-XML file: " + xpath);
            return null;
        }
        return result;
    }
}

/**
 * This is a small helper class that implements a generic context. It will be called with the .item-XML file in order to get all necessary namespaces from it.
 */
class TalendNamespaceContext implements NamespaceContext {

    private Document sourceDoc;

    TalendNamespaceContext(Document doc) {
        this.sourceDoc = doc;
    }

    @Override
    public String getNamespaceURI(String s) {
        if(s.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
            return this.sourceDoc.lookupNamespaceURI(null);
        }
        return this.sourceDoc.lookupNamespaceURI(s);
    }

    @Override
    public String getPrefix(String s) {
        return this.sourceDoc.lookupNamespaceURI(s);
    }

    @Override
    public Iterator<? extends Object> getPrefixes(String s) {
        return null;
    }
}
