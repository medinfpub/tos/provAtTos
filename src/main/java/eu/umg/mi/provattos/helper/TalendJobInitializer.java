package eu.umg.mi.provattos.helper;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;
import java.util.Properties;

import eu.umg.mi.provattos.componentmaps.ProvenanceEndComponentMap;
import eu.umg.mi.provattos.componentmaps.ProvenanceStartComponentMap;
import eu.umg.mi.provattos.modules.TalendActivityHandler;
import eu.umg.mi.provattos.modules.TalendEntityHandler;
import eu.umg.mi.provattos.modules.TalendFilenameParser;
import routines.system.api.TalendJob;

public class TalendJobInitializer {
    /**
     * Initializes all required libraries and other classpath targets for the target TalendJob
     * @param jobInfo a Properties instance that includes the job properties (in order to find the files using the job name and version)
     * @param tosPath a File instance with a path pointing to the (extracted) TOS job files
     * @return an anonymous class that corresponds to the target TalendJob
     */
    public static Class<?> initTosJobClass(Properties jobInfo, File tosPath) {
        String jobName = jobInfo.getProperty("job");
        // Create the binary name of the jobs primary class as well as the name of the .jar file containing it for convenience
        String jobJarFilename = jobName.toLowerCase() + "_" + jobInfo.getProperty("jobVersion").replace(".", "_");
        String jobBinaryName = jobInfo.getProperty("project").toLowerCase() + "." + jobJarFilename + "." + jobName;
        jobJarFilename = jobJarFilename + ".jar";

        // Load all the libraries as well as the jar of the target class
        File[] libs = new File(tosPath.getAbsolutePath() + File.separator + "lib").listFiles();
        // +2 as the target jar as well as the directory of the target jar will be added to classpath
        if(libs == null) {
            System.err.println("Unable to list the libraries, some internal error occurred.");
            return null;
        }
        URL[] libUrls = new URL[libs.length+2];
        File targetJarFile = new File(tosPath.getAbsolutePath() + File.separator + jobName + File.separator + jobJarFilename);
        int i = 0;
        try {
            // Here, all jars are referenced using urls in order to get loaded afterwards by URLClassLoader
            for(i = 0; i < libs.length; i++) {
                libUrls[i] = libs[i].toURI().toURL();
            }
            // Add the target class jar to the classpath
            libUrls[i] = targetJarFile.toURI().toURL();
            // Add the target path to the classpath, otherwise the context(s) wont load
            libUrls[i+1] = new File(tosPath.getAbsolutePath() + File.separator + jobName + "/").toURI().toURL();
        } catch(MalformedURLException mue) {
            // One of the jars are either referenced wrong or some other weird error happened
            String failedUri = i < libs.length ? libs[i].toURI().toString() : targetJarFile.toURI().toString();
            System.err.println("One of the java libraries path is malformed and cannot be parsed to a URL: " + failedUri);
            return null;
        }
        // finally, load the classes from the URLs
        URLClassLoader ucl = URLClassLoader.newInstance(libUrls);
        try {
            return ucl.loadClass(jobBinaryName);
        } catch(ClassNotFoundException cnfe) {
            System.err.println("Was not able to load the jobs primary class: " + jobBinaryName);
            return null;
        }
    }

    /**
     * Initializes an TalendJob instance from an anonymous TalendJob class. Basically this method catches a lot of exceptions, which makes the other code a lot more readable.
     * @param tosJob An anonymous class of a TalendJob
     * @param jobInfo A Properties instance holding the jobProperties values
     * @return A constructed instance of the provided anonymous TalendJob class
     */
    public static TalendJob initTosJobInstance(Class<?> tosJob, Properties jobInfo) {
        // Now the class of the job will be loaded and executed
        TalendJob tosJobInstance;
        String jobBinaryName = jobInfo.getProperty("project").toLowerCase() 
                + "." + jobInfo.getProperty("job").toLowerCase() + "_" + jobInfo.getProperty("jobVersion").replace(".", "_") 
                + "." + jobInfo.getProperty("job");
        try {
            // Load the class from the classLoader and confirm that we have a TalendJob
            if(TalendJob.class.isAssignableFrom(tosJob)) {
                // Construct the class
                Constructor<?> tosJobConstructor = tosJob.getConstructor();
                tosJobInstance = TalendJob.class.cast(tosJobConstructor.newInstance());
            } else {
                System.err.println("Found a class " + jobBinaryName + ", but it does not implement a TalendJob.");
                throw new ClassNotFoundException();
            }
            // There are a bunch of exceptions that can be thrown, the usually refer to problems loading the class or constructing an instance of it
        } catch(ClassNotFoundException cnfe) {
            System.err.println("Was not able to load the jobs primary class: " + jobBinaryName);
            return null;
        } catch (NoSuchMethodException e) {
            System.err.println("I was not able to get a constructor from the class " + jobBinaryName);
            return null;
        } catch (IllegalAccessException e) {
            System.err.println("I am not able to access the constructor from the class " + jobBinaryName);
            return null;
        } catch (InstantiationException e) {
            System.err.println("I am not able to instantiate the constructor from the class "+ jobBinaryName);
            return null;
        } catch (InvocationTargetException e) {
            System.err.println("I am not able to invoke the constructor from the class "+ jobBinaryName);
            return null;
        }
        return tosJobInstance;
    }

    /**
     * Initializes the modified maps and sets them in the Talend job process
     * @param tosJob the TalendJob class, which has been loaded earlier (using initTosJobClass) 
     * @param tosJobInstance a TalendJob instance created from the loaded class (using initTosJobInstance)
     * @param provHelper An instance of the ProvModelHelper, will be used in the component maps
     * @param xmlHelper An instance of the XMLHelper, will be used in the component maps
     * @return A reference to the globalMap
     */
    public static Map<?, ?> initTOSMaps(Class<?> tosJob, TalendJob tosJobInstance, ProvModelHelper provHelper, XMLHelper xmlHelper) {
        // use maps to store references to fields of a TalendJob that will be accessed
        Field[] maps = new Field[3];
        Map<?, ?> globalMap = null;
        try {
            if(provHelper == null) {
                System.err.println("The provenance helper has not been initialized yet. This looks like an internal error!");
                return null;
            }
            // get all the interesting maps
            maps[0] = tosJob.getDeclaredField("globalMap");
            maps[1] = tosJob.getDeclaredField("start_Hash");
            maps[2] = tosJob.getDeclaredField("end_Hash");
            // set all of them accessible
            for(Field f : maps) {
                f.setAccessible(true);
            }
            globalMap = (Map<?, ?>) maps[0].get(tosJobInstance);

            // apparently, TOS defines two contexts; one for the Default context and another one for non-default contexts
            // init the context properties object
            boolean isDefContext = tosJob.getDeclaredField("isDefaultContext").getBoolean(tosJobInstance);
            Field contextField;
            if(isDefContext) {
                contextField = tosJob.getDeclaredField("defaultProps");
            } else {
                contextField = tosJob.getDeclaredField("context");
            }
            contextField.setAccessible(true);

            // create the handlers for Activities (for TOS components) and Entities (for Input/Output operations) regarding TOS jobs
            TalendActivityHandler tActHandle = new TalendActivityHandler(provHelper, xmlHelper, globalMap);
            TalendEntityHandler tEntHandle = new TalendEntityHandler(provHelper, xmlHelper, new TalendFilenameParser((Properties) contextField.get(tosJobInstance), globalMap));

            // for the start and end hash-map a new class has been created that will store provenance directly while executing the job
            ProvenanceStartComponentMap<String, Long> start_Hash = new ProvenanceStartComponentMap<>(tActHandle);
            maps[1].set(tosJobInstance, start_Hash);
            ProvenanceEndComponentMap<String, Long> end_Hash = new ProvenanceEndComponentMap<>(tActHandle, tEntHandle);
            maps[2].set(tosJobInstance, end_Hash);

            // catch some exceptions that may occur while accessing a TalendJob instance
        } catch (NoSuchFieldException e) {
            System.err.println("One of the maps is not available.");
            return null;
        } catch (IllegalAccessException e) {
            System.err.println("Unable to access the maps. Cannot create provenance data.");
            return null;
        }
        return globalMap;
    }
}