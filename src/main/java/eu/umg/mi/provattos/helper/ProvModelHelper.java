package eu.umg.mi.provattos.helper;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.openprovenance.prov.model.ActedOnBehalfOf;
import org.openprovenance.prov.model.Activity;
import org.openprovenance.prov.model.Agent;
import org.openprovenance.prov.model.Document;
import org.openprovenance.prov.model.Entity;
import org.openprovenance.prov.model.HasLabel;
import org.openprovenance.prov.model.HasOther;
import org.openprovenance.prov.model.HasRole;
import org.openprovenance.prov.model.HasTime;
import org.openprovenance.prov.model.LangString;
import org.openprovenance.prov.model.Name;
import org.openprovenance.prov.model.Namespace;
import org.openprovenance.prov.model.ProvFactory;
import org.openprovenance.prov.model.QualifiedName;
import org.openprovenance.prov.model.Role;
import org.openprovenance.prov.model.StatementOrBundle;
import org.openprovenance.prov.model.Type;
import org.openprovenance.prov.model.Used;
import org.openprovenance.prov.model.WasAssociatedWith;
import org.openprovenance.prov.model.WasGeneratedBy;
import org.openprovenance.prov.model.WasInformedBy;

public class ProvModelHelper {

    enum QnNamespace {
        TALEND, JAVA, CUSTOM
    }

    private static final String DEFAULT_NAMESPACE = "http://www.example.com/";
    private static final int SALTLENGTH = 5;

    private Namespace ns;
    private ProvFactory pFac;
    private List<Entity> pEntities;
    private List<Activity> pActivities;
    private List<Agent> pAgents;
    private List<WasInformedBy> pCommunications;
    private List<WasGeneratedBy> pGenerations;
    private List<ActedOnBehalfOf> pDelegations;
    private List<WasAssociatedWith> pAssociations;
    private List<Used> pUsages;

    private long nextFreeId;
    private String salt;

    public ProvModelHelper() {
        this(null);
    }
    public ProvModelHelper(String customNamespace) {
        this.ns = new Namespace();
        this.pFac = new org.openprovenance.prov.xml.ProvFactory();
        this.ns.addKnownNamespaces();
        this.ns.register("talend", "http://www.talend.com/");
        this.ns.register("java", "https://www.java.com/");
        if(customNamespace == null) {
            this.ns.registerDefault(DEFAULT_NAMESPACE);
        } else {
            this.ns.registerDefault(customNamespace);
        }
        this.pEntities= new ArrayList<>();
        this.pActivities = new ArrayList<>();
        this.pAgents = new ArrayList<>();
        this.pCommunications = new ArrayList<>();
        this.pGenerations = new ArrayList<>();
        this.pDelegations = new ArrayList<>();
        this.pAssociations = new ArrayList<>();
        this.pUsages = new ArrayList<>();

        this.setNewIds(ProvModelHelper.SALTLENGTH);
    }


    private void setNewIds(int length) {
        this.nextFreeId = 0L;
        Random rand = new Random();
        String literals = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < length; i++) {
            sb.append(literals.charAt(rand.nextInt(literals.length() - 1)));
        }
        this.salt = sb.toString();
    }

    /**
     * Builds a list of StatementOrBundle elements that are saved in this helper class
     * @return A list of StatementOrBundle elements that are recorded in this helper class
     */
    private List<StatementOrBundle> getpElements() {
        List<StatementOrBundle> elements = new ArrayList<>();
        elements.addAll(pEntities);
        elements.addAll(pActivities);
        elements.addAll(pAgents);
        elements.addAll(pCommunications);
        elements.addAll(pGenerations);
        elements.addAll(pDelegations);
        elements.addAll(pAssociations);
        elements.addAll(pUsages);
        return elements;
    }

    /**
     * Returns a unique id as a String
     * 
     * Returns an id as a String based on a randomly generated String, consecutively numbered. This should be used to assign unique ids to provenance statements without using things like the timestamp in milliseconds.
     * 
     * @return A String concatenated from a random Sting and a number
     */
    public String getId() {
        long returnId = this.nextFreeId;
        this.nextFreeId++;
        if(this.nextFreeId < returnId) {
            this.setNewIds(ProvModelHelper.SALTLENGTH);
        }
        return this.salt + "_" + returnId;
    }

    /**
     * @deprecated
     * Gets a new or existing activity with the id key.
     * 
     * If key is already present within the existent activities, this function will return the existing activity from the list. Otherwise, a new activity will be created and returned.
     * 
     * @param key String representation of a key which will be used to create a QualifiedName
     * @return An Activity instance, a new one if it was not present within the list before
     */
    public Activity getActivity(String key) {
        Activity act = null;
        for(Activity a: this.pActivities) {
            if(a.getId().equals(this.getQualifiedTalendName(key))) {
                act = a;
            }
        }
        if(act == null) {
            act = this.pFac.newActivity(this.getQualifiedTalendName(key), key);
            this.pActivities.add(act);
        }
        return act;
    }

    /**
     * Creates a new activity for a TOS component.
     * 
     * Creates a new activity with the id key and the descriptor label for TOS components. There will be no check whether an Activitiy with the same id already exists.
     * 
     * @param key String representation of a key which will be used to create a QualifiedName
     * @param label A label for the newly generated activity
     * @return A created Activity instance
     */
    public Activity createActivity(String key, String label) {
        Activity act = this.pFac.newActivity(this.getCustomName(key), label);
        this.pActivities.add(act);
        return act;
    }

    /**
     * Find an Activity by id.
     * 
     * Searches for an Activity with the id. If no Activity matches, null will be returned.
     * 
     * @param key String representation of the key which has been used to create the QualifiedName.
     * @return A Activity instance with the id key or null if no Activity with key was found.
     */
    public Activity findActivityById(String key) {
        for(Activity a: this.pActivities) {
            if(a.getId().equals(this.getCustomName(key))) {
                return a;
            }
        }
        return null;
    }

    /**
     * Find an Activity by label.
     * 
     * Searches an Activity with the label that is assigned to it during creation. If no Activitiy matches, an empty list will be returned.
     * 
     * @param label String representation of the label which will be searched.
     * @return A List of Activities with label.
     */
    public List<Activity> findActivityByLabel(String label) {
        List<Activity> acts = new ArrayList<Activity>();
        for(Activity a: this.pActivities) {
            for(LangString ls: a.getLabel()) {
                if(ls.getValue().equals(label)) {
                    acts.add(a);
                }
            }
        }
        return acts;
    }

    public List<Activity> findActivityByLabel(Pattern label) {
        List<Activity> acts = new ArrayList<Activity>();
        for(Activity a: this.pActivities) {
            for(LangString ls: a.getLabel()) {
                Matcher m = label.matcher(ls.getValue());
                if(m.find()) {
                    acts.add(a);
                }
            }
        }
        return acts;
    }

    /**
     * If key is already present within the existent entities, this function will return the existing entity from the list. Otherwise, a new entity will be created and returned.
     * @param key String representation of a key which will be used to create a QualifiedName
     * @return An Entity instance, a new one if it was not present within the list before
     */
    public Entity getEntity(String key) {
        Entity ent = null;
        for(Entity e: this.pEntities) {
            if(e.getId().equals(this.getCustomName(key))) {
                ent = e;
            }
        }
        if(ent == null) {
            ent = this.pFac.newEntity(this.getCustomName(key));
            this.pEntities.add(ent);
        }
        return ent;
    }

    public WasInformedBy createNewConnectionToActivity(String connectionName, Activity target, String informantName) {
        return this.getCommunication(connectionName, target, this.getExtremeActivity(informantName, 1));
    }

    public WasInformedBy getCommunication(String name, Activity target, Activity informant) {
        WasInformedBy wib = null;
        if(target == null || informant == null || name == null) {
            return wib;
        }
        for(WasInformedBy w: this.pCommunications) {
            if(w.getId().equals(this.getQualifiedTalendName(name))) {
                wib = w;
            }
        }
        if(wib == null) {
            wib = pFac.newWasInformedBy(this.getQualifiedTalendName(name), target.getId(), informant.getId());
            this.pCommunications.add(wib);
        }
        return wib;
    }

    public WasGeneratedBy getGeneration(Activity act, Entity ent, Long time) {
        WasGeneratedBy wgn = null;
        for(WasGeneratedBy w: this.pGenerations) {
            if(w.getActivity().equals(act.getId()) && w.getEntity().equals(ent.getId())) {
                wgn = w;
            }
        }
        if(wgn == null) {
            wgn = this.pFac.newWasGeneratedBy(null, ent.getId(), act.getId());
            wgn = (WasGeneratedBy) this.setTime(wgn, time);
            this.pGenerations.add(wgn);
        }
        return wgn;
    }

    public List<Entity> getGeneratedBy(Activity act) {
        List<Entity> ents = new ArrayList<>();
        for(WasGeneratedBy w: this.pGenerations) {
            if(w.getActivity().equals(act.getId())) {
                ents.add(this.getEntity(w.getEntity().getLocalPart()));
            }
        }
        return ents;
    }

    public Used getUsage(Activity act, Entity ent, Long time) {
        Used use = this.getUsage(act, ent);
        return (Used) this.setTime(use, time);
    }

    private HasTime setTime(HasTime hT, Long time) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(new Date(time));
        try {
            hT.setTime(DatatypeFactory.newInstance().newXMLGregorianCalendar(gc));
        } catch(DatatypeConfigurationException e) {
            System.err.println("An exception during the setting of the time has occurred. No start- and end-times will be recorded within the provenance output: " + e.getMessage());
        }
        return hT;
    }

    public Used getUsage(Activity act, Entity ent) {
        Used use = null;
        for(Used u: this.pUsages) {
            if(u.getActivity().equals(act.getId()) && u.getEntity().equals(ent.getId())) {
                use = u;
            }
        }
        if(use == null) {
            use = this.pFac.newUsed(act.getId(), ent.getId());
            this.pUsages.add(use);
        }
        return use;
    }

    public void addOther(HasOther stmt, String name, String value, QualifiedName type) {
        this.addOther(stmt, this.getQualifiedTalendName(name), value, type);
    }

    public void addOther(HasOther stmt, QualifiedName name, String value, QualifiedName type) {
        stmt.getOther().add(pFac.newOther(name, value, type));
    }

    public void addOthers(HasOther stmt, Map<QualifiedName, String> others) {
        for(QualifiedName k: others.keySet()) {
            stmt.getOther().add(pFac.newOther(k, others.get(k), this.getName().XSD_STRING));
        }
    }

    public void addPlan(Entity ent) {
        Type type = this.pFac.newType("Plan", this.pFac.getName().PROV_TYPE);
        for(Type t: ent.getType()) {
            if(t.equals(type)) {
                return;
            }
        }
        ent.getType().add(type);
    }

    public void addLabel(HasLabel stmt, String name) {
        LangString langString = this.pFac.newInternationalizedString(name);
        for(LangString ls: stmt.getLabel()) {
            if(ls.equals(langString)) {
                return;
            }
        }
        stmt.getLabel().add(langString);
    }

    public void addRole(HasRole stmt, String roleName) {
        Role role = this.pFac.newRole(roleName, this.getName().XSD_STRING);
        for(Role r: stmt.getRole()) {
            if(r.equals(role)) {
                return;
            }
        }
        stmt.getRole().add(role);
    }

    public Name getName() {
        return this.pFac.getName();
    }

    /**
     * This functions adds Agents for Talend Open Studio, Java and the OS including the association of those agents to the job
     */
    public void addAgents(Properties jobInfo, XMLHelper xmlHelper) {
        Agent tosAgent = this.createAgent("TalendOpenStudio", QnNamespace.TALEND);
        Map<QualifiedName, String> tosProps = new HashMap<>();
        jobInfo.forEach((o, o2) -> {
            tosProps.put(this.getQualifiedTalendName((String) o), ((String) o2).replace("\\", "&#92;"));
        });
        this.addOthers(tosAgent, tosProps);
        Activity startAct = this.getExtremeActivity(xmlHelper.getStartingActivity(), -1);
        WasAssociatedWith waw = this.pFac.newWasAssociatedWith(this.getQualifiedTalendName("runJobInTos"), startAct.getId(), tosAgent.getId());
        this.pAssociations.add(waw);

        Agent jvmAgent = this.createAgent("JavaVirtualMachine", QnNamespace.JAVA);
        Map<QualifiedName, String> jvmProps = new HashMap<>();
        System.getProperties().forEach((o, o2) -> {
            if(o.toString().startsWith("java.") && !"java.class.path".equals(o))
                jvmProps.put(this.getQualifiedJavaName((String) o), ((String) o2).replace("\\", "&#92;"));
        });
        this.addOthers(jvmAgent, jvmProps);

        ActedOnBehalfOf abo = this.pFac.newActedOnBehalfOf(this.getCustomName("RunsOn"), tosAgent.getId(), jvmAgent.getId());
        this.pDelegations.add(abo);

        Agent os = this.createAgent("OperatingSystem", QnNamespace.CUSTOM);
        Map<QualifiedName, String> osProps = new HashMap<>();
        System.getProperties().forEach((o, o2) -> { 
            if(o.toString().startsWith("os."))
                osProps.put(this.getQualifiedJavaName((String) o), ((String) o2).replace("\\", "&#92;")); 
        });
        this.addOthers(os, osProps);

        abo = this.pFac.newActedOnBehalfOf(this.getCustomName("RunsOn"), jvmAgent.getId(), os.getId());
        this.pDelegations.add(abo);
    }

    private Agent createAgent(String name, QnNamespace ns) {
        Agent age = this.pFac.newAgent(this.getQn(name, ns), name);
        this.pAgents.add(age);
        return age;
    }

    public Document getDocument() {
        Document pDoc = this.pFac.newDocument();
        pDoc.getStatementOrBundle().addAll(this.getpElements());
        pDoc.setNamespace(this.ns);
        return pDoc;
    }

    /**
     * Returns the TOS UniqueName of an Activity
     * 
     * Returns the UniqueName of the component, which is recorded as a Activity, based on the labels that are assigned to the activities while creation.
     * 
     * @param act The activity, which unique name will be returned
     * 
     * @return The Unique Name for act
     */
    public static String activityToUniqueName(Activity act) {
        if(act.getLabel().size() > 1) {
            System.out.println("This should not happen! One Activity has multiple Labels.");
        }
        if(act.getLabel().size() == 1) {
            return act.getLabel().get(0).getValue();
        }
        return null;
    }

    /**
     * Returns the most extreme Activity with the Unique Name uniqueName. The modifier will determine which extreme will be used: -1 for "oldest" and 1 for "latest"
     * @param uniqueName The Unique Name of the activity
     * @param modifier Modifies the comparison, -1 (or any negative) for "oldest" and 1 (or any positive) for "latest"
     * @return The oldest or most recent Activity named uniqueName
     */
    public Activity getExtremeActivity(String uniqueName, int modifier) {
        Activity act = null;
        XMLGregorianCalendar mostExtreme = null;
        // iterate through the activities and get the most recent activity that matches uniqueName
        for(Activity a: this.pActivities) {
            if(ProvModelHelper.activityToUniqueName(a).equals(uniqueName)) {
                if(mostExtreme == null || a.getEndTime().compare(mostExtreme)*modifier > 0) {
                    mostExtreme = a.getEndTime();
                    act = a;
                }
            }
        }
        return act;
    }

    /**
     * Helper function to retrieve the fully-qualified name for the Talend namespace for a key
     * @param name The key that should be created withing the Talend namespace
     * @return The fully-qualified name for the Talend namespace
     */
    private QualifiedName getQualifiedTalendName(String name) {
        return ns.qualifiedName("talend", name, this.pFac);
    }

    /**
     * Helper function to retrieve the fully-qualified name for the Java namespace for a key
     * @param name The key that should be created withing the Java namespace
     * @return The fully-qualified name for the Java namespace
     */
    private QualifiedName getQualifiedJavaName(String name) {
        return ns.qualifiedName("java", name, this.pFac);
    }

    /**
     * Helper function to retrieve the fully-qualified name for the Custom namespace for a key
     * @param name The key that should be created withing the Custom namespace
     * @return The fully-qualified name for the Custom namespace
     */
    private QualifiedName getCustomName(String name) {
        return ns.qualifiedName(null, name, this.pFac);
    }

    /**
     * Helper function to retrieve the correct fully-qualified name
     * @param name The key which shall be retrieved
     * @param ns The target namespace, TALEND, JAVA or CUSTOM are available (null as well for default)
     * @return The correct QualifiedName for the called Namespace
     */
    private QualifiedName getQn(String name, QnNamespace ns) {
        switch(ns) {
            case TALEND:
                return this.getQualifiedTalendName(name);
            case JAVA:
                return this.getQualifiedJavaName(name);
            case CUSTOM:
            default:
                return this.getCustomName(name);
        }
    }
}
