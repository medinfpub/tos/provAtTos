package eu.umg.mi.provattos.componentmaps;

import java.util.HashMap;

import org.openprovenance.prov.model.Activity;

import eu.umg.mi.provattos.modules.TalendActivityHandler;
import eu.umg.mi.provattos.modules.TalendEntityHandler;

public class ProvenanceEndComponentMap<K, V> extends HashMap<K, V> {

    static final long serialVersionUID = 20180608L;
    private TalendActivityHandler tActHandle;
    private TalendEntityHandler tEntHandle;

    public ProvenanceEndComponentMap(TalendActivityHandler tActHandle, TalendEntityHandler tEntHandle) {
        super();
        this.tActHandle = tActHandle;
        this.tEntHandle = tEntHandle;
    }

    @Override
    public V put(K key, V value) {
        // make sure that this class is used from a endHash of a TalendJob
        if(key instanceof String && value instanceof Long) {
            // remark: although it is possible that the Long will not represent a timestamp, the resulting exception is captured and handled gracefully. The job will still run.
            try {
                Activity act = this.tActHandle.endActivity((String) key, (Long) value);
                if(act != null) {
                    // if this activity is relevant regarding entities, create an entity for it
                    if(this.tActHandle.hasRelevantEntity(act)) {
                        this.tEntHandle.createEntityReference(act, (Long) value);
                    }
                } else {
                    System.err.println("Tried to record ending times for not findable activity: "+key);
                }
            } catch(NullPointerException npe) {
                System.err.println("Recording provenance failed for " + key.toString());
            }
        }
        return super.put(key, value);
    }
}
