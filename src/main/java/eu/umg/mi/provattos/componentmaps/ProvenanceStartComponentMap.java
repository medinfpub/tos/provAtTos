package eu.umg.mi.provattos.componentmaps;

import java.util.HashMap;

import eu.umg.mi.provattos.modules.TalendActivityHandler;

public class ProvenanceStartComponentMap<K, V> extends HashMap<K, V> {

    static final long serialVersionUID = 20180608L;
    private TalendActivityHandler tActHandle;

    public ProvenanceStartComponentMap(TalendActivityHandler tActHandle) {
        super();
        this.tActHandle = tActHandle;
    }

    @Override
    public V put(K key, V value) {
        if(key instanceof String && value instanceof Long) {
            try {
                this.tActHandle.startActivity((String) key, (Long) value);
            } catch(NullPointerException npe) {
                System.err.println("Recording provenance failed for " + key.toString());
            }
        }
        return super.put(key, value);
    }
}
