package eu.umg.mi.provattos.modules;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.openprovenance.prov.model.Activity;
import org.openprovenance.prov.model.WasInformedBy;

import eu.umg.mi.provattos.helper.ProvModelHelper;
import eu.umg.mi.provattos.helper.XMLHelper;

public class TalendActivityHandler {

    private ProvModelHelper provHelper;
    private XMLHelper xmlHelper;
    private Map<?, ?> globalMap;
    private List<Activity> startedActivites;

    public TalendActivityHandler(ProvModelHelper provHelper, XMLHelper xmlHelper, Map<?, ?> globalMap) {
        this.provHelper = provHelper;
        this.xmlHelper = xmlHelper;
        this.globalMap = globalMap;

        this.startedActivites = new ArrayList<Activity>();
    }

    /**
     * Starts (and creates) a new Activity with an unique name a starting time
     * @param uniqueName The unique name of the TOS component for which a Activity will be started
     * @param timestamp The starting time
     */
    public void startActivity(String uniqueName, long timestamp) {
        Activity act = this.provHelper.createActivity("act_" + this.provHelper.getId(), uniqueName);
        act.setStartTime(this.longToXmlGregorianCalendar(timestamp));

        // add it to the started activities
        this.startedActivites.add(act);
        this.addParametersTo(act);
    }

    /**
     * Ends an activity which has been started earlier, based on the unique name
     * @param uniqueName unique name of the to-be-ended activity
     * @param timestamp ending time of the activity
     * @return the Activity which has been ended
     */
    public Activity endActivity(String uniqueName, long timestamp) {
        // retrieve the activity from the started activities
        Activity act = null;
        for(Activity a: this.startedActivites) {
            if(ProvModelHelper.activityToUniqueName(a).equals(uniqueName)) {
                act = a;
            }
        }
        if(act != null) {
            // remove the activity from the started list
            startedActivites.remove(act);

            // set ending timestamp and create communication
            act.setEndTime(this.longToXmlGregorianCalendar(timestamp));
            this.createCommunication(act);
            return act;
        } else {
            System.err.println("Tried to end not-started activity: " + uniqueName);
        }
        return null;
    }

    /**
     * Creates all connection based on the input rows of act using the createNewConnectionToActivity method of ProvModelHelper (informedBy connections)
     * @param act The inputs of this activity will be used to determine which connections to create
     */
    public void createCommunication(Activity act) {
        // First, get all sources for the input rows using the getConnectedFrom method
        List<org.w3c.dom.NamedNodeMap> sources = this.xmlHelper.getConnectedFrom(ProvModelHelper.activityToUniqueName(act));
        if(sources == null) {
            // check if a tPreJob exists
            Activity preJob = null;
            for(Activity a: this.provHelper.findActivityByLabel(Pattern.compile("^tPrejob_[0-9]+$"))) {
                if(preJob != null) {
                    if(preJob.getStartTime().compare(a.getStartTime()) < 0) {
                        preJob = a;
                    }
                } else {
                    preJob = a;
                }
            }
            if(!ProvModelHelper.activityToUniqueName(act).startsWith("tPrejob") && preJob != null) { // are multiple proJobs possible?
                // If an activity has no incoming connections and a tPreJob exists, the target of the tPrejob component communicates with the starting component
                WasInformedBy wib = provHelper.getCommunication("OnSubjobOk_" + act.getStartTime().toGregorianCalendar().getTimeInMillis(), act, preJob);
            }
            return;
        }
        for(org.w3c.dom.NamedNodeMap source: sources) {
            // the row label and the name of the source activity is contained in the NamedNodeMap  (corresponds to attributes from an XML element in .item
            org.w3c.dom.Node rowLabel = source.getNamedItem("label");
            org.w3c.dom.Node sourceName = source.getNamedItem("source");
            if(rowLabel != null && sourceName != null) {
                // TODO: dirty! find another solution to this
                if(sourceName.getNodeValue().startsWith("tAggregateRow") && !sourceName.getNodeValue().endsWith("_AGGOUT")) {
                    sourceName.setNodeValue(sourceName.getNodeValue() + "_AGGIN");
                }
                // create the informedBy connection between the source and the target activity
                WasInformedBy wib = provHelper.createNewConnectionToActivity(rowLabel.getNodeValue() + "_" + act.getStartTime().toGregorianCalendar().getTimeInMillis(), act, sourceName.getNodeValue());
                if(wib != null) {
                    // if created an informedBy connection, set the line number that the row handled (if available)
                    Object nbLineObj = this.globalMap.get(sourceName.getNodeValue() + "_NB_LINE");
                    if (nbLineObj != null) {
                        provHelper.addLabel(wib, rowLabel.getNodeValue());
                        provHelper.addOther(wib, "NB_LINE", nbLineObj.toString(), provHelper.getName().XSD_INT);
                    }
                }
            }
        }
    }

    /**
     * Retrieves the parameters for the activity from the .item-XML file, setting them as "Other" to the activity. Some parameters are returned in order to put them into entities (as they are too big, e.g. sourcecode)
     * @param act The activity for which the parameters will be retrieved
     */
    public void addParametersTo(Activity act) {
        String key = ProvModelHelper.activityToUniqueName(act);
        if(key.contains("tAggregateRow")) {
            if(key.endsWith("_AGGIN")) {
                key = key.substring(0, key.length() - "_AGGIN".length());
            } else if(key.endsWith("_AGGOUT")) {
                key = key.substring(0, key.length() - "AGGOUT".length());
            }
        }
        Properties elementProps = this.xmlHelper.getNodeElementParameters(key);
        for(String propKey: elementProps.stringPropertyNames()) {
            String cleanPropKey = propKey;
            if(propKey.contains(":")) {
                cleanPropKey = propKey.replace(":", "_").replace("\\", "&#92;");
            }
            this.provHelper.addOther(act, cleanPropKey, elementProps.getProperty(propKey), this.provHelper.getName().XSD_STRING);
        }
    }

    /**
     * Checks whether an activity should refer to entities based on its unique name
     * @param act The activity that will be checked for entities
     * @return True if the activity should refer to entities, false otherwise
     */
    public boolean hasRelevantEntity(Activity act) {
        // key is the local part of the QN aka the Unique name of a TOS component
        String key = ProvModelHelper.activityToUniqueName(act);
        if(key.contains("Input") || key.contains("Output") || key.contains("tMap") || this.xmlHelper.getNodeJavaCodeParameters(key).size() > 0) {
            return true;
        }
        return false;
    }

    private XMLGregorianCalendar longToXmlGregorianCalendar(long timestamp) {
        GregorianCalendar startTime = new GregorianCalendar();
        startTime.setTime(new Date(timestamp));
        try {
            XMLGregorianCalendar datetime = DatatypeFactory.newInstance().newXMLGregorianCalendar(startTime);
            return datetime;
        } catch (DatatypeConfigurationException e) {
            System.err.println("An exception during the setting of the time has occurred. No start- and end-times will be recorded within the provenance output: " + e.getMessage());
        }
        return null;
    }
}