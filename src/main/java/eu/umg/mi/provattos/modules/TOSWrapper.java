package eu.umg.mi.provattos.modules;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;
import java.util.Properties;

import javax.naming.ConfigurationException;

import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.interop.InteropFramework.ProvFormat;
import routines.system.api.TalendJob;
import eu.umg.mi.provattos.helper.*;
import eu.umg.mi.provattos.modules.TalendJobPropertiesParser;

public class TOSWrapper {

    // Fields for the TOS job that will be run
    private File tosPath;
    private boolean zipped = false;
    private Properties jobInfo;

    // Fields that help in accessing required HashMaps of a TOS job
    private Map<?, ?> globalMap;

    // references to helper classes
    private ProvModelHelper provHelper;
    private XMLHelper xmlHelper;

    // Fields that help dealing with output files
    private File outPath;
    private ProvFormat provFormat = ProvFormat.PROVN;
    private String defNamespace;

    // errors will be captures here. May be deleted in the future as it seems bad practice, but I will see I guess
    private boolean erroneous = false;

    public boolean isErroneous() {
        return erroneous;
    }

    /**
     * Constructs the general Talend Open Studio job wrapper, either directly from a zip-file (by unzipping it using the UnzipHelper) or from the already extracted files.
     *
     * @param filename Path to a exported TOS-job in a .zip file or the unzipped directory (the directory with the jobInfo.properties file in it)
     */
    public TOSWrapper(String filename) {
        this.tosPath = new File(filename);
        // is the provided path a zip file?
        if (this.tosPath.isFile() && this.tosPath.getName().endsWith(".zip")) {
            try {
                this.tosPath = UnzipHelper.unzip(this.tosPath);
            } catch (IOException e) {
                System.err.println("Error occured during unzip:\n" + e.getMessage());
                this.erroneous = true;
            }
            this.zipped = true;
        // if it is not a zip file, it should contain a "jobInfo.properties" file. Check that
        }
        if(this.tosPath == null || !this.tosPath.isDirectory() || !(new File(this.tosPath.getAbsolutePath() + File.separator + "jobInfo.properties")).isFile()) {
            this.erroneous = true;
            System.err.println("Is your target an exported Talend Open Studio job? (The (extracted) directory is missing a jobInfo.properties file)");
        }
    }

    /**
     * Set the output directory for output files like the provenance data files.
     * @param output path to the to-be-written output directory
     */
    public void initOutputFile(File output) {
        try {
            this.outPath = output.getCanonicalFile();
            this.outPath.mkdirs();
        } catch (IOException e) {
            System.err.println("The absolute pathname could not be constructed. Something weird filesystem-wise has happened!");
            this.erroneous = true;
            return;
        }
    }

    /**
     * This function uses a simple switch to set the provenance format the output will be saved to.
     * @param provFormat String representation of the desired format, usually provided by a commandline argument
     */
    public void setProvenanceFormat(String provFormat) {
        switch(provFormat.toLowerCase()) {
            case "provo":
            case "prov-o":
            case "turtle":
            case "ttl":
            case "owl":
                this.provFormat = ProvFormat.TURTLE;
                break;
            case "xml":
            case "prov-xml":
            case "provxml":
                this.provFormat = ProvFormat.XML;
                break;
            case "provn":
            case "prov-n":
            case "notation":
            default:
                this.provFormat = ProvFormat.PROVN;
                break;
        }
    }

    public void setDefaultNamespace(String ns) {
        this.defNamespace = ns;
    }

    /**
     * Initializes the job properties and .item-XML filesm which are used extensively afterwards
     */
    private void initJobProperties() {
        // construct the File object to the jobInfo.properties file
        File jobInfoFile = new File(this.tosPath.getAbsolutePath() + File.separator + "jobInfo.properties");
        this.jobInfo = TalendJobPropertiesParser.parseTalendProperties(jobInfoFile);
    }

    /**
     * Initializes the XML helper, mainly using the jobInfo properties file (to find the .item file)
     */
    private void initXMLHelper() {
        // Now also load the items xml file of the job (this is handled by the xmlHelper)
        String jobName = this.jobInfo.getProperty("job");
        // this path is a starting point, as it is not documented where the xml-file will reside (this depends on the folder structure within TOS)
        File pathToItemFile = new File(this.tosPath + File.separator
                + jobName + File.separator
                + "items" + File.separator
                + this.jobInfo.getProperty("project").toLowerCase() + File.separator
                + "process" + File.separator);

        File itemFileName = new File(jobName + "_" + this.jobInfo.getProperty("jobVersion") + ".item");
        this.xmlHelper = new XMLHelper(this.outPath, pathToItemFile, itemFileName);
    }

    /**
     * Runs an initialized TOSWrapper. This will initialize all necessary TOS classes and instances and run a job.
     * @param args String arguments that will be transferred to the talend job (e.g. context specifications)
     */
    public void run(String[] args) throws ConfigurationException {
        if(this.erroneous) {
            System.err.println("An error has been already thrown while initializing. Cannot run.");
            return;
        }

        this.initJobProperties();
        if(this.erroneous) {
            throw new ConfigurationException("Job properties could not be initialized.");
        }
        this.initXMLHelper();
        if(this.erroneous) {
            throw new ConfigurationException(".item-XML file could not be initialized.");
        }

        // init the class including all classpath variables
        Class<?> tosJob = TalendJobInitializer.initTosJobClass(this.jobInfo, this.tosPath);
        if(tosJob == null) {
            return;
        }

        // init a TalendJob instance from the previously initialized class
        TalendJob tosJobInstance = TalendJobInitializer.initTosJobInstance(tosJob, this.jobInfo);
        if(tosJobInstance == null) {
            return;
        }

        // init the provenance helper to simplify capturing provenance information
        this.provHelper = new ProvModelHelper(defNamespace);

        // init accessing the three important HashMaps of a TOS job
        this.globalMap = TalendJobInitializer.initTOSMaps(tosJob, tosJobInstance, this.provHelper, this.xmlHelper);
        if(this.globalMap == null) {
            return;
        }

        // run the job and capture the exit code. Important: from this point on, the function should not return on error but do proper cleanups
        int exitCode = tosJobInstance.runJobInTOS(args);
        System.out.println("TOS Job exited with code: " + exitCode);
        this.provHelper.addAgents(this.jobInfo, this.xmlHelper);

        // create a prov document using the helper and write it to the filesystem
        String provFilename = this.outPath.getAbsolutePath() + File.separator 
                + this.jobInfo.getProperty("job") + "_" + this.jobInfo.getProperty("jobVersion").replace(".", "_")
                + "." + this.provFormat.toString();
        (new InteropFramework()).writeDocument(provFilename, this.provFormat, provHelper.getDocument());
    }

    /**
     * Cleans up everything that has been opened or created during execution.
     */
    public void close() {
        // Erase the unzipped directory (if unzipped previously)
        if (this.zipped) {
            // This is copied from the java 8 API: https://docs.oracle.com/javase/8/docs/api/index.html?java/nio/file/FileVisitor.html
            try {
                Files.walkFileTree(this.tosPath.toPath(), new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        // some files (especially bigger files) may be in use even after the job has finished. Hence, retry deleting three times including a little waiting time
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException e) {
                        // if e != null, an exception from visitFile has been thrown. This exception is already handled.
                        try {
                            Files.delete(dir);
                        } catch(IOException ioe) {
                            // do not error out here
                        }
                        return FileVisitResult.CONTINUE;
                    }

                });
            } catch (IOException e) {
                System.err.println("For some reason, the temporary unzipped files could not be removed:\n" + e.getMessage());
            }
        }
    }
}