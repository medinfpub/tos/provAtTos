package eu.umg.mi.provattos.modules;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TalendJobPropertiesParser {

    /**
     * Parses a provided jobProperties file to a java.util.Properties instance
     * @param filename A File instance pointing to the jobProperties file created in a TOS job
     * @return The jobProperties values parsed into a Properties instance
     */
    public static Properties parseTalendProperties(File filename) {
        Properties jobInfo = new Properties();

        try {
            // read and parse the file, store it to a Properties object
            BufferedReader jobInfoReader = new BufferedReader(new FileReader(filename));
            jobInfo = new Properties();
            String line;
            while ((line = jobInfoReader.readLine()) != null) {
                String[] props = line.split("=", 2);
                if(props.length == 2) {
                    jobInfo.setProperty(props[0], props[1]);
                }
            }
            jobInfoReader.close();

        } catch (FileNotFoundException e) {
            System.err.println("The jobInfo.properties could not be found: " + filename.toString());
            return null;
        } catch (IOException e) {
            System.err.println("Could not read the jobInfo.properties file: " + filename.toString());
            return null;
        }
        return jobInfo;
    }
}