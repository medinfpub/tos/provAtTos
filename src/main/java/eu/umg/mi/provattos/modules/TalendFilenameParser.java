package eu.umg.mi.provattos.modules;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TalendFilenameParser {
    private Properties context;
    private Map<?, ?> globalMap;

    public TalendFilenameParser(Properties contextProps, Map<?, ?> globalMap) {
        this.context = contextProps;
        this.globalMap = globalMap;
    }

    public String parse(String nonParsed) {

        // if the value is surrounded with brackets, remove those
        nonParsed = nonParsed.trim();
        if(nonParsed.startsWith("(") && nonParsed.endsWith(")")) {
            return nonParsed.substring(1, nonParsed.length()-1);
        }

        // create a StringBuilder for the result
        StringBuilder parsed = new StringBuilder();
        // create a "temporary" StringBuilder that may be evaluated and recreated within the method
        StringBuilder sb = new StringBuilder();
        // create a stack for handling of control characters (like ")
        Stack<Character> st = new Stack<>();
        // create a list for parts of method call that are separated by .
        List<String> ls = new ArrayList<>();

        for(char c: nonParsed.toCharArray()) {
            if(c == '"') {
                // handle quotes
                sb.append(c);
                if(!st.empty() && st.peek() == '"') {
                    st.pop();
                } else {
                    st.push('"');
                }

            } else if(!st.empty() && st.peek() == '"') {
                // within quotes: everything is a string, just append
                sb.append(c);

            } else if (c == '(') {
                // brackets open a function (outside of quotes)
                sb.append(c);
                st.push('(');

            } else if(c == ')') {
                // closing brackets, try to pop an opening bracket; if not possible, the string is malformed
                sb.append(c);
                if(!st.empty() && st.peek() == '(') {
                    st.pop();
                } else {
                    System.err.println("Syntax Error in String");
                    return null;
                }

            } else if (!st.empty() && st.peek() == '(') {
                // ? is that needed?
                sb.append(c);

            } else if(c == '.') {
                // split the string on a dot outside of a string and add a new part to the list ls
                ls.add(sb.toString().trim());
                sb = new StringBuilder();

            } else if (c == '+') {
                // on a +, evaluate the already captured string and move on afterwards (outside of a string of course)
                ls.add(sb.toString().trim());
                sb = new StringBuilder();
                parsed.append(this.evaluate(ls));
                ls = new ArrayList<>();

            } else {
                // every other case: just append the string
                sb.append(c);
            }
        }

        // handle the end of the string like a '+'
        ls.add(sb.toString().trim());
        parsed.append(this.evaluate(ls));
        return parsed.toString();
    }

    private String evaluate(List<String> evalString) {
        // with a size of 0, return empty string
        if(evalString.size() <= 0) {
            return "";
        }

        if(evalString.size() == 1) {
            String str = evalString.get(0);

            // if str is null, then return an empty string
            if(str == null) {
                return "";
            }
            // if this is a String, return right away
            if(str.startsWith("\"") && str.endsWith("\"") && StringUtils.countMatches(str, '"') == 2) {
                return str.substring(1, str.length() -1);
            }

            // if only one element and not a string, just return it; dunno what that is to be honest
            return str;
        }

        // on more than 1 element, regex will be used in some way
        Pattern p;
        Matcher m;

        // handle two cases that are well-known
        if(evalString.get(0).equals("context")) {
            // get the value from a context
            evalString.remove(0);
            evalString.set(0, this.context.getProperty(evalString.get(0)));
            return this.evaluate(evalString);
        }
        if(evalString.get(0).equals("globalMap")) {
            // get the value from the globalMap
            if(evalString.get(1).substring(0, 4).equals("get(")) {
                p = Pattern.compile("\\([^)]+\\)");
                m = p.matcher(evalString.get(1));
                if(!m.find()) {
                    System.err.println("The regex pattern did not match, either the String is malformed or the regex pattern is false!");
                    return null;
                }
                String key = m.group();
                key = key.substring(2, key.length()-2);
                evalString.set(1, (String) this.globalMap.get(key));
                evalString.remove(0);
                return this.evaluate(evalString);
            }
        }

        // other cases: evaluate string functions, like Substring
        // TODO: do that, try to handle functions generically with the reflect API
        return null;
    }
}
