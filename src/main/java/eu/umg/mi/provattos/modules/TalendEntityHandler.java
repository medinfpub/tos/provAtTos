package eu.umg.mi.provattos.modules;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.codec.digest.DigestUtils;
import org.openprovenance.prov.model.Activity;
import org.openprovenance.prov.model.Entity;
import org.openprovenance.prov.model.Used;
import org.openprovenance.prov.model.WasGeneratedBy;

import eu.umg.mi.provattos.helper.ProvModelHelper;
import eu.umg.mi.provattos.helper.XMLHelper;

public class TalendEntityHandler {

    private XMLHelper xmlHelper;
    private ProvModelHelper provHelper;
    private TalendFilenameParser parser;
    private String jobName;

    public TalendEntityHandler(ProvModelHelper provHelper, XMLHelper xmlHelper, TalendFilenameParser parser) {
        this.xmlHelper = xmlHelper;
        this.provHelper = provHelper;
        this.parser = parser;

        String theJobName = xmlHelper.getFilename();
        if(theJobName != null) {
            this.jobName = theJobName.substring(0, theJobName.length() - ".item".length()).replace(".", "_");
        } else {
            this.jobName = "unkownJob";
        }
    }

    /**
     * Accepts an reference to an Activity (represents the component, that used / created an entity) and a timestamp
     * @param act Reference to the activity that involved an (yet unknown) entity
     * @param timestamp A timestamp indicating when it creation / usage happened
     */
    public void createEntityReference(Activity act, long timestamp) {
        String uniqueName = ProvModelHelper.activityToUniqueName(act);
        // basically, this function handles individual cases of input / output components by matching the unique_names

        // handle input and output entities
        if(uniqueName.contains("FileOutput") || uniqueName.contains("HashOutput")) {
            Entity ent = this.createInOutEntity(act, "_output");
            if(ent != null) {
                // will catch the created generation here, although it may not be needed.
                WasGeneratedBy wgb = this.provHelper.getGeneration(act, ent, timestamp);
            }
        }
        if(uniqueName.contains("FileInput")) {
            Entity ent = this.createInOutEntity(act, "_input");
            if (ent != null) {
                // will catch the created usage here, although it may not be needed.
                Used use = this.provHelper.getUsage(act, ent, timestamp);
            }
        }
        if(uniqueName.contains("tHashInput")) {
            for(Entity e: this.getConnectedHashComponents(uniqueName)) {
                this.provHelper.getUsage(act, e, timestamp);
            }
        }
        if(uniqueName.contains("tMap")) {
            Entity ent = this.createPlanForMap(act);
            if(ent != null) {
                // will catch the created usage here, although it may not be needed.
                Used use = this.provHelper.getUsage(act, ent, timestamp);
            }
        }
        Properties javaProps = this.xmlHelper.getNodeJavaCodeParameters(uniqueName);
        if(javaProps.size() > 0) {
            for(String key: javaProps.stringPropertyNames()) {
                Entity ent = this.createSourcecode(act, javaProps.getProperty(key));
                if(ent != null) {
                    // will catch created usage here, although it may not be needed.
                    Used use = this.provHelper.getUsage(act, ent, timestamp);
                }
            }
        }
    }

    private Entity createPlanForMap(Activity act) {
        File entFile = this.xmlHelper.extractPlanForMap(ProvModelHelper.activityToUniqueName(act), this.jobName);
        return this.createInOutEntity(act, entFile, "_plan");
    }

    public Entity createSourcecode(Activity act, String code) {
        File sourceFile = new File(this.xmlHelper.getOutputPath().getAbsolutePath()
                + File.separator + ProvModelHelper.activityToUniqueName(act) + "_code.java");
        try {
            PrintWriter sourceFileWriter = new PrintWriter(sourceFile);
            sourceFileWriter.print(code);
            sourceFileWriter.close();
        } catch(FileNotFoundException e) {
            System.err.println("Could not create a file to write java code in it for activity " + ProvModelHelper.activityToUniqueName(act));
        }
        return this.createInOutEntity(act, sourceFile, "_plan");
        
    }
    /**
     * Creates an input or output entity, which both are basically the same procedure. It will get the filename parameters using the getFilenameByNodeName method which will be parsed using the parseFilename method. If possible, a (SHA-1) hash of the file is used to create an Entity name that is unique.
     * @param act The activity that uses or generates an entity
     * @param suffix A suffix that will be appended to the name of the activity if creating the entity using a hash failed
     * @return the created entity that represents an input / output within TOS
     */
    private Entity createInOutEntity(Activity act, String suffix) {
        // get the filename from the properties.item XML file
        String filename = this.getFilenameByNodeName(ProvModelHelper.activityToUniqueName(act));
        // parse the filename, replace "known" characters / parts directly
        filename = filename.replace("&quot;", "\"").replace("(String)", "");
        filename = this.parseFilename(filename);
        // try to create file for the name
        File outputEntityFile = new File(filename);
        return this.createInOutEntity(act, outputEntityFile, suffix);
    }

    private Entity createInOutEntity(Activity act, File outputEntityFile, String suffix) {
        String entLabel = ProvModelHelper.activityToUniqueName(act) + suffix + "_" + System.currentTimeMillis();
        String entName = DigestUtils.sha256Hex(entLabel);

        // if exists, get the SHA-256 hash of the file in order to use it as the name of the entity; otherwise, use a generic action + suffix name as a label
        String hashValue = this.getSha256Of(outputEntityFile);
        if(hashValue != null) {
            entName = hashValue;
            entLabel = outputEntityFile.getName();
        }
        // create the entity, label and return it
        Entity ent = this.provHelper.getEntity(entName);
        this.provHelper.addLabel(ent, entLabel);
        if(suffix.equals("_plan")) {
            this.provHelper.addPlan(ent);
        }
        return ent;
    }

    /**
     * Little helper function that retrieves a SHA-256 Hash of a targetFile or null if File cannot be reached
     */
    private String getSha256Of(File targetFile) {
        String hash = null;
        if(targetFile != null && targetFile.exists() && targetFile.canRead()) {
            try {
                byte[] input = Files.readAllBytes(targetFile.toPath());
                hash = DigestUtils.sha256Hex(input);
            } catch (IOException e) {
                // may happen, do not error out here.
            }
        }
        return hash;
    }

    /**
     * Parses a filename that can be given by the properties.item XML-file from TOS. Pure String representations are handled correctly, "simple" calls using the context or the globalMap are also recognized. Concatenated String using '+' are handled aaswell.
     * @param nonparsed The non-parsed String representation from the properties.item XML-file
     * @return A string representation of the parsed filename, "simple" context and globalMap variables are replaced
     */
    private String parseFilename(String nonparsed) {
        return this.parser.parse(nonparsed);
    }

    /**
     * Gets the filename value from the properties.items XML-file that corrosponds to the node "nodeName"
     * @param nodeName String representation of the unique name of the node
     * @return String of the filename setting that is set in the component
     */
    private String getFilenameByNodeName(String nodeName) {
        return this.xmlHelper.getElementByNodeName(nodeName, "FILENAME");
    }

    /**
     * Handles HashInput components such that a list of Entities is returned for every entity that has been generated by HashOutput components that are linked with the HashInput component named nodeName
     * @param nodeName The unique name of the HashInput component
     * @return A list of entities that will be used by the HashInput component named nodeName
     */
    private List<Entity> getConnectedHashComponents(String nodeName) {
        String[] components = this.xmlHelper.getElementByNodeName(nodeName, "LIST").split(",");
        List<Entity> entities = new ArrayList<>();
        for(String s: components) {
            Activity act = this.provHelper.getExtremeActivity(s, 1);
            entities.addAll(this.provHelper.getGeneratedBy(act));
        }
        return entities;
    }
}