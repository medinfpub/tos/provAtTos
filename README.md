[![pipeline status](https://gitlab.gwdg.de/medinfpub/tos/provAtTos/badges/master/pipeline.svg)](https://gitlab.gwdg.de/medinfpub/tos/provAtTos/commits/master) Master branch

[![pipeline status](https://gitlab.gwdg.de/medinfpub/tos/provAtTos/badges/dev/pipeline.svg)](https://gitlab.gwdg.de/medinfpub/tos/provAtTos/commits/dev) Development branch

[![pipeline status](https://gitlab.gwdg.de/medinfpub/tos/provAtTos/badges/release_0-1/pipeline.svg)](https://gitlab.gwdg.de/medinfpub/tos/provAtTos/commits/release_0-1) Release 0.1 branch

# PROV@TOS
A W3C Prov Wrapper for Talend Open Studio for Data Integration Jobs

## Getting and compiling the software
This project compiles best with Maven. To do so, clone this repository:

```bash
git clone https://gitlab.gwdg.de/medinfpub/tos/provAtTos
```

Afterwards, compile it using maven using `mvn package` in the cloned folder. It will compile all files to the `target` folder.
There may be other ways to start it using maven, I just used this exact method to compile the project. Any suggestions (via an issue) to improve this method are appreciated.

## Running PROV@TOS

To wrap around an existing Talend Open Studio for Data Integration job, export it using the "Build Job"-function (accessible in the context menu of a job under "Job Designs"). The "Context script" as well as the "Items" are mandatory, as they are used in the wrapper.

Run it with the command:

```bash
java -jar target/provAtTos.jar -o /some/output/dir /my/talend/job.zip
```

Without an output (specified with the `-o` or `--output` argument), the current working dir will be used to save the file that need to be created (e.g. a provenance file in the desired format, XML-files with MapperData from tMap components, ...). 
The format of the provenance data can be changed using the `-f` (or `--format`). Available formats are: `xml`, `turtle`, `provn` (which is the default):

```bash
java -jar target/provAtTos.jar -f turtle /my/talend/job.zip
```

It is possible to add additional arguments, which will be then provided to the Talend job itself, using the -t (or --tos) option:

```bash
java -jar target/provAtTos.jar -t "--context_param=Default" /my/talend/job.zip
```

A complete example could look like this:

```bash
java -jar target/provAtTos.jar -o /home/username/Documents/provenanceOutput -f xml -t "--context_param=Testing" /home/username/workspace/dataIntegrationJob.zip
```

## Licence
Copyright 2018 Department for Medical Informatics, Unviersity Medical Center Göttingen, Germany

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at
	http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

Check the [dependencies](dependencies.md) overview for an automatically created (using Apache Maven) list of all dependencies including their licensing.
