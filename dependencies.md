

# Project Dependencies


## compile
The following is a list of compile dependencies for this project. These dependencies are required to compile and run the application:


| GroupId | ArtifactId | Version | Type | Licenses |
| --- | --- | --- | --- | --- |
| commons-cli | commons-cli | 1.4 | jar | Apache License, Version 2.0 |
| org.openprovenance.prov | prov-interop | 0.7.3 | jar | license.txt |
| org.openprovenance.prov | prov-model | 0.7.3 | jar | license.txt |
| org.talend.esb.job | org.talend.esb.job.api | 7.0.1 | jar | Apache Software License - Version 2.0 |



# Project Transitive Dependencies
The following is a list of transitive dependencies for this project. Transitive dependencies are the dependencies of the project dependencies.


## compile
The following is a list of compile dependencies for this project. These dependencies are required to compile and run the application:


| GroupId | ArtifactId | Version | Type | Licenses |
| --- | --- | --- | --- | --- |
| antlr | antlr | 2.7.7 | jar | BSD License |
| com.fasterxml.jackson.core | jackson-annotations | 2.7.5 | jar | The Apache Software License, Version 2.0 |
| com.fasterxml.jackson.core | jackson-core | 2.7.5 | jar | The Apache Software License, Version 2.0 |
| com.fasterxml.jackson.core | jackson-databind | 2.7.5 | jar | The Apache Software License, Version 2.0 |
| com.fasterxml.jackson.dataformat | jackson-dataformat-xml | 2.7.5 | jar | The Apache Software License, Version 2.0 |
| com.fasterxml.jackson.module | jackson-module-jaxb-annotations | 2.7.5 | jar | The Apache Software License, Version 2.0 |
| com.google.code.gson | gson | 2.1 | jar | The Apache Software License, Version 2.0 |
| com.google.guava | guava | 19.0 | jar | The Apache Software License, Version 2.0 |
| com.squareup | javapoet | 1.7.0 | jar | Apache 2.0 |
| com.sun.xml.bind | jaxb-core | 2.2.11 | jar | CDDL+GPL License |
| com.sun.xml.bind | jaxb-impl | 2.2.11 | jar | CDDL+GPL License |
| commons-codec | commons-codec | 1.9 | jar | The Apache Software License, Version 2.0 |
| commons-collections | commons-collections | 3.2.2 | jar | Apache License, Version 2.0 |
| commons-httpclient | commons-httpclient | 3.1 | jar | Apache License |
| commons-io | commons-io | 2.4 | jar | The Apache Software License, Version 2.0 |
| commons-lang | commons-lang | 2.6 | jar | The Apache Software License, Version 2.0 |
| javax.xml.bind | jaxb-api | 2.2.12 | jar | CDDL 1.1 | - | GPL2 w/ CPE |
| log4j | log4j | 1.2.17 | jar | The Apache Software License, Version 2.0 |
| org.antlr | antlr-runtime | 3.4 | jar | - |
| org.antlr | stringtemplate | 4.0.2 | jar | BSD licence |
| org.apache.commons | commons-lang3 | 3.4 | jar | Apache License, Version 2.0 |
| org.codehaus.woodstox | stax2-api | 3.1.4 | jar | The BSD License |
| org.jboss.resteasy | jaxrs-api | 3.0.8.Final | jar | Apache License, Version 2.0 |
| org.openprovenance.prov | prov-dot | 0.7.3 | jar | license.txt |
| org.openprovenance.prov | prov-generator | 0.7.3 | jar | license.txt |
| org.openprovenance.prov | prov-json | 0.7.3 | jar | license.txt |
| org.openprovenance.prov | prov-n | 0.7.3 | jar | license.txt |
| org.openprovenance.prov | prov-rdf | 0.7.3 | jar | license.txt |
| org.openprovenance.prov | prov-template | 0.7.3 | jar | license.txt |
| org.openprovenance.prov | prov-xml | 0.7.3 | jar | license.txt |
| org.openrdf.sesame | sesame-http-client | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-http-protocol | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-model | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-query | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryalgebra-evaluation | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryalgebra-model | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryparser-api | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryresultio-api | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryresultio-sparqlxml | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-repository-api | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-repository-contextaware | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-repository-event | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-repository-http | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-repository-manager | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-repository-sail | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-repository-sparql | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-rio-api | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-rio-n3 | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-rio-ntriples | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-rio-rdfxml | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-rio-trig | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-rio-turtle | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-runtime | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-sail-api | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-sail-inferencer | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-sail-memory | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-util | 2.6.10 | jar | Aduna BSD license |
| org.slf4j | slf4j-api | 1.6.1 | jar | MIT License |
| xerces | xercesImpl | 2.11.0 | jar | The Apache Software License, Version 2.0 |
| xml-apis | xml-apis | 1.4.01 | jar | The Apache Software License, Version 2.0 | - | The SAX License | - | The W3C License |



## runtime
The following is a list of runtime dependencies for this project. These dependencies are required to run the application:


| GroupId | ArtifactId | Version | Type | Licenses |
| --- | --- | --- | --- | --- |
| commons-dbcp | commons-dbcp | 1.3 | jar | The Apache Software License, Version 2.0 |
| commons-pool | commons-pool | 1.5.4 | jar | The Apache Software License, Version 2.0 |
| net.sf.opencsv | opencsv | 2.0 | jar | Apache 2 |
| org.openrdf.sesame | sesame-queryparser-serql | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryparser-sparql | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryresultio-binary | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryresultio-sparqljson | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-queryresultio-text | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-repository-dataset | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-rio-binary | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-rio-trix | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-sail-nativerdf | 2.6.10 | jar | Aduna BSD license |
| org.openrdf.sesame | sesame-sail-rdbms | 2.6.10 | jar | Aduna BSD license |



# Project Dependency Graph


## Dependency Tree

* eu.umg.mi:provAtTos:jar:0.1 


  * org.openprovenance.prov:prov-interop:jar:0.7.3 (compile)   


    * org.openprovenance.prov:prov-xml:jar:0.7.3 (compile)     


      * com.sun.xml.bind:jaxb-impl:jar:2.2.11 (compile)       


      * com.sun.xml.bind:jaxb-core:jar:2.2.11 (compile)       


      * xerces:xercesImpl:jar:2.11.0 (compile)       


        * xml-apis:xml-apis:jar:1.4.01 (compile)         


      * commons-lang:commons-lang:jar:2.6 (compile)       


    * org.openprovenance.prov:prov-n:jar:0.7.3 (compile)     


      * org.antlr:antlr-runtime:jar:3.4 (compile)       


        * antlr:antlr:jar:2.7.7 (compile)         


      * org.antlr:stringtemplate:jar:4.0.2 (compile)       


    * org.openprovenance.prov:prov-dot:jar:0.7.3 (compile)     


    * org.openprovenance.prov:prov-json:jar:0.7.3 (compile)     


      * com.google.code.gson:gson:jar:2.1 (compile)       


    * org.openprovenance.prov:prov-rdf:jar:0.7.3 (compile)     


      * org.openrdf.sesame:sesame-runtime:jar:2.6.10 (compile)       


        * org.openrdf.sesame:sesame-model:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-query:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-queryalgebra-model:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-queryparser-api:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-queryparser-serql:jar:2.6.10 (runtime)         


        * org.openrdf.sesame:sesame-queryparser-sparql:jar:2.6.10 (runtime)         


        * org.openrdf.sesame:sesame-queryresultio-api:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-queryresultio-binary:jar:2.6.10 (runtime)         


        * org.openrdf.sesame:sesame-queryresultio-sparqljson:jar:2.6.10 (runtime)         


        * org.openrdf.sesame:sesame-queryresultio-sparqlxml:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-queryresultio-text:jar:2.6.10 (runtime)         


          * net.sf.opencsv:opencsv:jar:2.0 (runtime)           


        * org.openrdf.sesame:sesame-repository-api:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-repository-manager:jar:2.6.10 (compile)         


          * org.openrdf.sesame:sesame-repository-event:jar:2.6.10 (compile)           


        * org.openrdf.sesame:sesame-repository-http:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-repository-sail:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-repository-dataset:jar:2.6.10 (runtime)         


        * org.openrdf.sesame:sesame-repository-contextaware:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-http-protocol:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-http-client:jar:2.6.10 (compile)         


          * commons-httpclient:commons-httpclient:jar:3.1 (compile)           


        * org.openrdf.sesame:sesame-rio-api:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-rio-binary:jar:2.6.10 (runtime)         


        * org.openrdf.sesame:sesame-rio-ntriples:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-rio-trix:jar:2.6.10 (runtime)         


        * org.openrdf.sesame:sesame-rio-turtle:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-sail-api:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-sail-inferencer:jar:2.6.10 (compile)         


        * org.openrdf.sesame:sesame-sail-memory:jar:2.6.10 (compile)         


          * org.openrdf.sesame:sesame-queryalgebra-evaluation:jar:2.6.10 (compile)           


            * org.openrdf.sesame:sesame-repository-sparql:jar:2.6.10 (compile)             


        * org.openrdf.sesame:sesame-sail-nativerdf:jar:2.6.10 (runtime)         


        * org.openrdf.sesame:sesame-sail-rdbms:jar:2.6.10 (runtime)         


          * commons-dbcp:commons-dbcp:jar:1.3 (runtime)           


            * commons-pool:commons-pool:jar:1.5.4 (runtime)             


        * org.slf4j:slf4j-api:jar:1.6.1 (compile)         


      * org.openrdf.sesame:sesame-rio-n3:jar:2.6.10 (compile)       


      * org.openrdf.sesame:sesame-rio-rdfxml:jar:2.6.10 (compile)       


        * org.openrdf.sesame:sesame-util:jar:2.6.10 (compile)         


      * org.openrdf.sesame:sesame-rio-trig:jar:2.6.10 (compile)       


    * org.openprovenance.prov:prov-template:jar:0.7.3 (compile)     


      * com.fasterxml.jackson.core:jackson-annotations:jar:2.7.5 (compile)       


      * com.fasterxml.jackson.dataformat:jackson-dataformat-xml:jar:2.7.5 (compile)       


        * com.fasterxml.jackson.core:jackson-core:jar:2.7.5 (compile)         


        * com.fasterxml.jackson.core:jackson-databind:jar:2.7.5 (compile)         


        * com.fasterxml.jackson.module:jackson-module-jaxb-annotations:jar:2.7.5 (compile)         


        * org.codehaus.woodstox:stax2-api:jar:3.1.4 (compile)         


      * com.squareup:javapoet:jar:1.7.0 (compile)       


      * com.google.guava:guava:jar:19.0 (compile)       


    * org.openprovenance.prov:prov-generator:jar:0.7.3 (compile)     


    * javax.xml.bind:jaxb-api:jar:2.2.12 (compile)     


    * org.jboss.resteasy:jaxrs-api:jar:3.0.8.Final (compile)     


    * log4j:log4j:jar:1.2.17 (compile)     


  * org.openprovenance.prov:prov-model:jar:0.7.3 (compile)   


    * org.apache.commons:commons-lang3:jar:3.4 (compile)     


    * commons-codec:commons-codec:jar:1.9 (compile)     


    * commons-collections:commons-collections:jar:3.2.2 (compile)     


    * commons-io:commons-io:jar:2.4 (compile)     


  * commons-cli:commons-cli:jar:1.4 (compile)   


  * org.talend.esb.job:org.talend.esb.job.api:jar:7.0.1 (compile)   




# Licenses
**Apache Software License - Version 2.0: **Talend ESB :: Job :: API
**The SAX License: **XML Commons External Components XML APIs
**CDDL 1.1: **Java Architecture for XML Binding
**The BSD License: **Stax2 API
**Apache 2.0: **JavaPoet
**MIT License: **PROV-DOT, PROV-GENERATOR, PROV-INTEROP, PROV-JSON, PROV-MODEL, PROV-N, PROV-RDF, PROV-TEMPLATE, PROV-XML, SLF4J API Module
**Apache License: **HttpClient
**The W3C License: **XML Commons External Components XML APIs
**Aduna BSD license: **OpenRDF Sesame: DatasetRepository (wrapper), OpenRDF Sesame: HTTP client, OpenRDF Sesame: HTTP protocol, OpenRDF Sesame: HTTPRepository, OpenRDF Sesame: Inferencer Sails, OpenRDF Sesame: MemoryStore, OpenRDF Sesame: Model, OpenRDF Sesame: NativeStore, OpenRDF Sesame: Query, OpenRDF Sesame: Query algebra - evaluation, OpenRDF Sesame: Query algebra - model, OpenRDF Sesame: Query parser - API, OpenRDF Sesame: Query parser - SPARQL, OpenRDF Sesame: Query parser - SeRQL, OpenRDF Sesame: Query result IO - API, OpenRDF Sesame: Query result IO - SPARQL/JSON, OpenRDF Sesame: Query result IO - SPARQL/XML, OpenRDF Sesame: Query result IO - binary, OpenRDF Sesame: Query result IO - plain text booleans, OpenRDF Sesame: RDBMS Sail, OpenRDF Sesame: Repository - API, OpenRDF Sesame: Repository - context aware (wrapper), OpenRDF Sesame: Repository - event (wrapper), OpenRDF Sesame: Repository manager, OpenRDF Sesame: Rio - API, OpenRDF Sesame: Rio - Binary, OpenRDF Sesame: Rio - N-Triples, OpenRDF Sesame: Rio - N3 (writer-only), OpenRDF Sesame: Rio - RDF/XML, OpenRDF Sesame: Rio - TriG, OpenRDF Sesame: Rio - TriX, OpenRDF Sesame: Rio - Turtle, OpenRDF Sesame: Runtime, OpenRDF Sesame: SPARQL Repository, OpenRDF Sesame: Sail API, OpenRDF Sesame: SailRepository, OpenRDF Sesame: util
**GPL2 w/ CPE: **Java Architecture for XML Binding
**BSD licence: **ANTLR StringTemplate 4.0.2
**Apache 2: **opencsv
**Unknown: **Antlr 3.4 Runtime
**BSD License: **AntLR Parser Generator
**Apache License, Version 2.0: **Apache Commons CLI, Apache Commons Collections, Apache Commons Lang, JAX-RS Core API, provAtTos
**CDDL+GPL License: **Old JAXB Core, Old JAXB Runtime
**The Apache Software License, Version 2.0: **Apache Commons Codec, Apache Log4j, Commons DBCP, Commons IO, Commons Lang, Commons Pool, Gson, Guava: Google Core Libraries for Java, Jackson-annotations, Jackson-core, Jackson-dataformat-XML, Jackson-module-JAXB-annotations, XML Commons External Components XML APIs, Xerces2-j, jackson-databind


# Dependency File Details


| Filename | Size | Entries | Classes | Packages | Java Version | Debug Information |
| --- | --- | --- | --- | --- | --- | --- |
| antlr-2.7.7.jar | 445.3 kB | 239 | 224 | 12 | 1.2 | Yes |
| jackson-annotations-2.7.5.jar | 50.9 kB | 72 | 60 | 1 | 1.6 | Yes |
| jackson-core-2.7.5.jar | 252.9 kB | 112 | 89 | 9 | 1.6 | Yes |
| jackson-databind-2.7.5.jar | 1.2 MB | 605 | 571 | 20 | 1.6 | Yes |
| jackson-dataformat-xml-2.7.5.jar | 94.8 kB | 68 | 46 | 6 | 1.6 | Yes |
| jackson-module-jaxb-annotations-2.7.5.jar | 34.6 kB | 30 | 12 | 3 | 1.6 | Yes |
| gson-2.1.jar | 180.1 kB | 158 | 148 | 6 | 1.5 | Yes |
| guava-19.0.jar | 2.3 MB | 1746 | 1717 | 17 | 1.6 | Yes |
| javapoet-1.7.0.jar | 88.4 kB | 45 | 35 | 1 | 1.7 | Yes |
| jaxb-core-2.2.11.jar | 252.3 kB | 251 | 175 | 23 | 1.6 | Yes |
| jaxb-impl-2.2.11.jar | 1 MB | 670 | 537 | 19 | 1.6 | Yes |
| commons-cli-1.4.jar | 53.8 kB | 40 | 27 | 1 | 1.5 | Yes |
| commons-codec-1.9.jar | 264 kB | 227 | 85 | 6 | 1.6 | Yes |
| commons-collections-3.2.2.jar | 588.3 kB | 484 | 460 | 12 | 1.3 | Yes |
| commons-dbcp-1.3.jar | 148.8 kB | 80 | 62 | 5 | 1.4 | Yes |
| commons-httpclient-3.1.jar | 305 kB | 183 | 167 | 8 | 1.2 | Yes |
| commons-io-2.4.jar | 185.1 kB | 128 | 110 | 6 | 1.6 | Yes |
| commons-lang-2.6.jar | 284.2 kB | 155 | 133 | 10 | 1.3 | Yes |
| commons-pool-1.5.4.jar | 96.2 kB | 66 | 52 | 2 | 1.3 | Yes |
| jaxb-api-2.2.12.jar | 102.5 kB | 122 | 104 | 6 | 1.5 | Yes |
| log4j-1.2.17.jar | 489.9 kB | 353 | 314 | 21 | 1.4 | Yes |
| opencsv-2.0.jar | 15.3 kB | 19 | 7 | 2 | 1.5 | Yes |
| antlr-runtime-3.4.jar | 164.4 kB | 128 | 115 | 4 | 1.4 | Yes |
| stringtemplate-4.0.2.jar | 226.4 kB | 139 | 125 | 5 | 1.4 | Yes |
| commons-lang3-3.4.jar | 434.7 kB | 248 | 224 | 12 | 1.6 | Yes |
| stax2-api-3.1.4.jar | 161.9 kB | 144 | 124 | 11 | 1.5 | Yes |
| jaxrs-api-3.0.8.Final.jar | 107.1 kB | 141 | 126 | 6 | 1.6 | Yes |
| prov-dot-0.7.3.jar | 49.9 kB | 38 | 19 | 1 | 1.7 | Yes |
| prov-generator-0.7.3.jar | 10.2 kB | 14 | 3 | 1 | 1.7 | Yes |
| prov-interop-0.7.3.jar | 29.4 kB | 22 | 10 | 1 | 1.7 | Yes |
| prov-json-0.7.3.jar | 27.9 kB | 18 | 7 | 1 | 1.7 | Yes |
| prov-model-0.7.3.jar | 112.1 kB | 97 | 84 | 3 | 1.7 | Yes |
| prov-n-0.7.3.jar | 167.2 kB | 70 | 59 | 1 | 1.7 | Yes |
| prov-rdf-0.7.3.jar | 75.3 kB | 30 | 14 | 2 | 1.7 | Yes |
| prov-template-0.7.3.jar | 74.2 kB | 47 | 16 | 1 | 1.7 | Yes |
| prov-xml-0.7.3.jar | 186.2 kB | 126 | 82 | 3 | 1.7 | Yes |
| sesame-http-client-2.6.10.jar | 18.4 kB | 14 | 3 | 1 | 1.5 | Yes |
| sesame-http-protocol-2.6.10.jar | 23.3 kB | 31 | 17 | 4 | 1.5 | Yes |
| sesame-model-2.6.10.jar | 84.6 kB | 63 | 48 | 7 | 1.5 | Yes |
| sesame-query-2.6.10.jar | 67.7 kB | 83 | 68 | 4 | 1.5 | Yes |
| sesame-queryalgebra-evaluation-2.6.10.jar | 210.8 kB | 163 | 139 | 11 | 1.5 | Yes |
| sesame-queryalgebra-model-2.6.10.jar | 132 kB | 113 | 101 | 2 | 1.5 | Yes |
| sesame-queryparser-api-2.6.10.jar | 10.6 kB | 21 | 10 | 1 | 1.5 | Yes |
| sesame-queryparser-serql-2.6.10.jar | 164.1 kB | 126 | 111 | 2 | 1.5 | Yes |
| sesame-queryparser-sparql-2.6.10.jar | 281.9 kB | 209 | 194 | 2 | 1.5 | Yes |
| sesame-queryresultio-api-2.6.10.jar | 16.3 kB | 29 | 18 | 1 | 1.5 | Yes |
| sesame-queryresultio-binary-2.6.10.jar | 13.2 kB | 21 | 6 | 1 | 1.5 | Yes |
| sesame-queryresultio-sparqljson-2.6.10.jar | 6.7 kB | 16 | 2 | 1 | 1.5 | Yes |
| sesame-queryresultio-sparqlxml-2.6.10.jar | 17 kB | 30 | 13 | 1 | 1.5 | Yes |
| sesame-queryresultio-text-2.6.10.jar | 19.4 kB | 31 | 12 | 3 | 1.5 | Yes |
| sesame-repository-api-2.6.10.jar | 37.6 kB | 39 | 26 | 4 | 1.5 | Yes |
| sesame-repository-contextaware-2.6.10.jar | 17.7 kB | 21 | 7 | 2 | 1.5 | Yes |
| sesame-repository-dataset-2.6.10.jar | 11.6 kB | 22 | 8 | 2 | 1.5 | Yes |
| sesame-repository-event-2.6.10.jar | 23 kB | 30 | 17 | 3 | 1.5 | Yes |
| sesame-repository-http-2.6.10.jar | 21.1 kB | 26 | 12 | 2 | 1.5 | Yes |
| sesame-repository-manager-2.6.10.jar | 24.6 kB | 22 | 10 | 2 | 1.5 | Yes |
| sesame-repository-sail-2.6.10.jar | 23.5 kB | 28 | 14 | 2 | 1.5 | Yes |
| sesame-repository-sparql-2.6.10.jar | 35.1 kB | 32 | 17 | 3 | 1.5 | Yes |
| sesame-rio-api-2.6.10.jar | 25.3 kB | 33 | 22 | 2 | 1.5 | Yes |
| sesame-rio-binary-2.6.10.jar | 11.3 kB | 19 | 5 | 1 | 1.5 | Yes |
| sesame-rio-n3-2.6.10.jar | 4.7 kB | 17 | 3 | 1 | 1.5 | Yes |
| sesame-rio-ntriples-2.6.10.jar | 13.6 kB | 19 | 5 | 1 | 1.5 | Yes |
| sesame-rio-rdfxml-2.6.10.jar | 32.8 kB | 28 | 13 | 2 | 1.5 | Yes |
| sesame-rio-trig-2.6.10.jar | 7 kB | 18 | 4 | 1 | 1.5 | Yes |
| sesame-rio-trix-2.6.10.jar | 11 kB | 20 | 6 | 1 | 1.5 | Yes |
| sesame-rio-turtle-2.6.10.jar | 17 kB | 19 | 5 | 1 | 1.5 | Yes |
| sesame-runtime-2.6.10.jar | 2.4 kB | 10 | 1 | 1 | 1.5 | No |
| sesame-sail-api-2.6.10.jar | 55.6 kB | 65 | 49 | 4 | 1.5 | Yes |
| sesame-sail-inferencer-2.6.10.jar | 21.8 kB | 26 | 11 | 3 | 1.5 | Yes |
| sesame-sail-memory-2.6.10.jar | 55.1 kB | 44 | 29 | 3 | 1.5 | Yes |
| sesame-sail-nativerdf-2.6.10.jar | 105 kB | 70 | 53 | 5 | 1.5 | Yes |
| sesame-sail-rdbms-2.6.10.jar | 293.4 kB | 219 | 185 | 18 | 1.5 | Yes |
| sesame-util-2.6.10.jar | 55.9 kB | 49 | 30 | 7 | 1.5 | Yes |
| slf4j-api-1.6.1.jar | 25.5 kB | 34 | 23 | 3 | 1.3 | Yes |
| org.talend.esb.job.api-7.0.1.jar | 12.2 kB | 24 | 13 | 1 | 1.6 | Yes |
| xercesImpl-2.11.0.jar | 1.4 MB | 1035 | 952 | 40 | 1.3 | No |
| xml-apis-1.4.01.jar | 220.5 kB | 391 | 346 | 29 | 1.1 | No |
| Total | Size | Entries | Classes | Packages | Java Version | Debug Information |
| --- | --- | --- | --- | --- | --- | --- |
| 77 | 13.9 MB | 10425 | 8751 | 433 | 1.7 | 74 |
| compile: 64 | compile: 12.7 MB | compile: 9507 | compile: 8048 | compile: 388 | - | compile: 61 |
| runtime: 13 | runtime: 1.2 MB | runtime: 918 | runtime: 703 | runtime: 45 | - | runtime: 13 |



# Dependency Repository Locations


| Repo ID | URL | Release | Snapshot |
| --- | --- | --- | --- |
| maven2-repository.dev.java.net | http://download.java.net/maven/2/ | Yes | Yes |
| central | https://repo.maven.apache.org/maven2 | Yes | No |
| jboss | http://repository.jboss.org/nexus/content/groups/public/ | Yes | Yes |
| sonatype-nexus-snapshots | https://oss.sonatype.org/content/repositories/snapshots | No | Yes |
| jvnet-nexus-staging | https://maven.java.net/content/repositories/staging/ | Yes | Yes |
| releases.java.net | https://maven.java.net/content/repositories/releases/ | Yes | Yes |
| apache.snapshots | http://repository.apache.org/snapshots | No | Yes |

Repository locations for each of the Dependencies.


| Artifact | maven2-repository.dev.java.net | central | jboss | sonatype-nexus-snapshots | jvnet-nexus-staging | releases.java.net | apache.snapshots |
| --- | --- | --- | --- | --- | --- | --- | --- |
| antlr:antlr:jar:2.7.7 | - | - | - | - | - | - |
| com.fasterxml.jackson.core:jackson-annotations:jar:2.7.5 | - | - | - | - | - | - |
| com.fasterxml.jackson.core:jackson-core:jar:2.7.5 | - | - | - | - | - | - |
| com.fasterxml.jackson.core:jackson-databind:jar:2.7.5 | - | - | - | - | - | - |
| com.fasterxml.jackson.dataformat:jackson-dataformat-xml:jar:2.7.5 | - | - | - | - | - | - |
| com.fasterxml.jackson.module:jackson-module-jaxb-annotations:jar:2.7.5 | - | - | - | - | - | - |
| com.google.code.gson:gson:jar:2.1 | - | - | - | - | - | - |
| com.google.guava:guava:jar:19.0 | - | - | - | - | - | - |
| com.squareup:javapoet:jar:1.7.0 | - | - | - | - | - | - |
| com.sun.xml.bind:jaxb-core:jar:2.2.11 | - | - | - | - |
| com.sun.xml.bind:jaxb-impl:jar:2.2.11 | - | - | - | - |
| commons-cli:commons-cli:jar:1.4 | - | - | - | - | - | - |
| commons-codec:commons-codec:jar:1.9 | - | - | - | - | - | - |
| commons-collections:commons-collections:jar:3.2.2 | - | - | - | - | - | - |
| commons-dbcp:commons-dbcp:jar:1.3 | - | - | - | - | - | - |
| commons-httpclient:commons-httpclient:jar:3.1 | - | - | - | - | - | - |
| commons-io:commons-io:jar:2.4 | - | - | - | - | - | - |
| commons-lang:commons-lang:jar:2.6 | - | - | - | - | - | - |
| commons-pool:commons-pool:jar:1.5.4 | - | - | - | - | - | - |
| javax.xml.bind:jaxb-api:jar:2.2.12 | - | - | - | - |
| log4j:log4j:jar:1.2.17 | - | - | - | - | - | - |
| net.sf.opencsv:opencsv:jar:2.0 | - | - | - | - | - | - |
| org.antlr:antlr-runtime:jar:3.4 | - | - | - | - | - | - |
| org.antlr:stringtemplate:jar:4.0.2 | - | - | - | - | - | - |
| org.apache.commons:commons-lang3:jar:3.4 | - | - | - | - | - | - |
| org.codehaus.woodstox:stax2-api:jar:3.1.4 | - | - | - | - | - | - |
| org.jboss.resteasy:jaxrs-api:jar:3.0.8.Final | - | - | - | - | - |
| org.openprovenance.prov:prov-dot:jar:0.7.3 | - | - | - | - | - | - |
| org.openprovenance.prov:prov-generator:jar:0.7.3 | - | - | - | - | - | - |
| org.openprovenance.prov:prov-interop:jar:0.7.3 | - | - | - | - | - | - |
| org.openprovenance.prov:prov-json:jar:0.7.3 | - | - | - | - | - | - |
| org.openprovenance.prov:prov-model:jar:0.7.3 | - | - | - | - | - | - |
| org.openprovenance.prov:prov-n:jar:0.7.3 | - | - | - | - | - | - |
| org.openprovenance.prov:prov-rdf:jar:0.7.3 | - | - | - | - | - | - |
| org.openprovenance.prov:prov-template:jar:0.7.3 | - | - | - | - | - | - |
| org.openprovenance.prov:prov-xml:jar:0.7.3 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-http-client:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-http-protocol:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-model:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-query:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryalgebra-evaluation:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryalgebra-model:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryparser-api:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryparser-serql:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryparser-sparql:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryresultio-api:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryresultio-binary:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryresultio-sparqljson:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryresultio-sparqlxml:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-queryresultio-text:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-repository-api:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-repository-contextaware:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-repository-dataset:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-repository-event:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-repository-http:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-repository-manager:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-repository-sail:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-repository-sparql:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-rio-api:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-rio-binary:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-rio-n3:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-rio-ntriples:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-rio-rdfxml:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-rio-trig:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-rio-trix:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-rio-turtle:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-runtime:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-sail-api:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-sail-inferencer:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-sail-memory:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-sail-nativerdf:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-sail-rdbms:jar:2.6.10 | - | - | - | - | - | - |
| org.openrdf.sesame:sesame-util:jar:2.6.10 | - | - | - | - | - | - |
| org.slf4j:slf4j-api:jar:1.6.1 | - | - | - | - | - | - |
| org.talend.esb.job:org.talend.esb.job.api:jar:7.0.1 | - | - | - | - | - | - |
| xerces:xercesImpl:jar:2.11.0 | - | - | - | - | - | - |
| xml-apis:xml-apis:jar:1.4.01 | - | - | - | - | - | - |
| Total | maven2-repository.dev.java.net | central | jboss | sonatype-nexus-snapshots | jvnet-nexus-staging | releases.java.net | apache.snapshots |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 77 (compile: 64, runtime: 13) | 0 | 77 | 1 | 0 | 3 | 3 | 0 |

